package com.intellicount.dao;

import com.intellicount.model.ProfileGoals.ProfileGoal;

public interface ProfileGoalDAO {
    ProfileGoal create(ProfileGoal profileGoal);
}
