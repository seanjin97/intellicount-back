package com.intellicount.dao;

import com.intellicount.model.Syllabus.Syllabus;

import java.util.List;

public interface SyllabusDAO {
    Syllabus findByChapterAndName(String chapter, String name);

    List<Syllabus> getAll();
}
