package com.intellicount.dao;

import com.intellicount.model.Option.Option;
import com.intellicount.repositories.OptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("postgresOption")
public class PostgresOptionDataAccessService implements OptionDAO{
    private final OptionRepository optionRepository;

    @Autowired
    public PostgresOptionDataAccessService(OptionRepository optionRepository) {
        this.optionRepository = optionRepository;
    }

    @Override
    public Option findById(UUID id) {
        return optionRepository.findById(id).orElse(null);
    }

    @Override
    public Option create(Option option) {
        return optionRepository.save(option);
    }
}
