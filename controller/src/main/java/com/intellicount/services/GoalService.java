package com.intellicount.services;

import com.intellicount.dao.GoalDAO;
import com.intellicount.model.Goal.Goal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class GoalService {
    private final GoalDAO goalDAO;

    @Autowired
    public GoalService(@Qualifier("postgresGoal") GoalDAO goalDAO) {
        this.goalDAO = goalDAO;
    }

    public Goal findByName(String name) {
        return goalDAO.findTopByName(name);
    }

    public Goal create(Goal goal) {
        return goalDAO.create(goal);
    }
}
