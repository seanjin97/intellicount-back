package com.intellicount.services;

import com.intellicount.dao.SessionDAO;
import com.intellicount.model.Session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class SessionService {
    private final SessionDAO sessionDAO;

    @Autowired

    public SessionService(@Qualifier("postgresSession") SessionDAO sessionDAO) {
        this.sessionDAO = sessionDAO;
    }

    public Session createSession(Session session) {
        return sessionDAO.createSession(session);
    }

    public Session findByCode(String code) {
        return sessionDAO.findByCode(code);
    }

    public Session findById(UUID id) {
        return sessionDAO.findById(id);
    }

    public void deleteSession(Session session) {
        sessionDAO.delete(session);
    }
}
