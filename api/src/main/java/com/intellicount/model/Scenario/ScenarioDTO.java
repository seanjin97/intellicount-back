package com.intellicount.model.Scenario;

import com.intellicount.model.DTO;
import com.intellicount.model.Decision.Decision;
import com.intellicount.model.Decision.DecisionDTO;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

public class ScenarioDTO implements Serializable, DTO {

    @NotBlank
    private String text;

    @NotBlank
    private String question;

    private Set<DecisionDTO> decisions;

    public ScenarioDTO() {
    }

    public ScenarioDTO(@NotBlank String text, @NotBlank String question, Set<DecisionDTO> decisions) {
        this.text = text;
        this.question = question;
        this.decisions = decisions;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Set<DecisionDTO> getDecisions() {
        return decisions;
    }

    public void setDecisions(Set<DecisionDTO> decisions) {
        this.decisions = decisions;
    }

    @Override
    public Scenario toTrueClass() {
        return new Scenario(
                this.text, this.question
        );
    }


}
