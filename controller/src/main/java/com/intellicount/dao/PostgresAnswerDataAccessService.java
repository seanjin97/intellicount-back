package com.intellicount.dao;

import com.intellicount.model.Answer.Answer;
import com.intellicount.repositories.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("postgresAnswer")
public class PostgresAnswerDataAccessService implements AnswerDAO{
    private final AnswerRepository answerRepository;

    @Autowired
    public PostgresAnswerDataAccessService(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    @Override
    public Answer create(Answer answer) {
        return answerRepository.save(answer);
    }
}
