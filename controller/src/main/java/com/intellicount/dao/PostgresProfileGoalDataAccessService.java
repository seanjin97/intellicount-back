package com.intellicount.dao;

import com.intellicount.model.ProfileGoals.ProfileGoal;
import com.intellicount.repositories.ProfileGoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("PostgresProfileGoal")
public class PostgresProfileGoalDataAccessService implements ProfileGoalDAO{
    private final ProfileGoalRepository profileGoalRepository;
    @Autowired
    public PostgresProfileGoalDataAccessService(ProfileGoalRepository profileGoalRepository) {
        this.profileGoalRepository = profileGoalRepository;
    }

    @Override
    public ProfileGoal create(ProfileGoal profileGoal) {
        return profileGoalRepository.save(profileGoal);
    }
}
