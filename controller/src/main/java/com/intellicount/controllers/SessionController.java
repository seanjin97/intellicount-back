package com.intellicount.controllers;

import com.intellicount.model.Admin.Admin;
import com.intellicount.model.Player.Player;
import com.intellicount.model.Question.Question;
import com.intellicount.model.QuestionList.QuestionList;
import com.intellicount.model.Session.Session;
import com.intellicount.model.Session.SessionCreateDTO;
import com.intellicount.model.Session.SessionJoinDTO;
import com.intellicount.services.AdminService;
import com.intellicount.services.PlayerService;
import com.intellicount.services.QuestionListService;
import com.intellicount.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class SessionController {

    private final SessionService sessionService;
    private final AdminService adminService;
    private final QuestionListService questionListService;
    private final PlayerService playerService;

    @Autowired
    public SessionController(SessionService sessionService, AdminService adminService, QuestionListService questionListService, PlayerService playerService) {
        this.sessionService = sessionService;
        this.adminService = adminService;
        this.questionListService = questionListService;
        this.playerService = playerService;
    }

    @PostMapping(value = "/admin/session/")
    public ResponseEntity<Session> createSession(@RequestBody SessionCreateDTO sessionCreateDTO) {
        Admin admin = adminService.findById(sessionCreateDTO.getAdmin());
        if (admin == null) {
            return ResponseEntity.notFound().build();
        }
        QuestionList questionList = questionListService.findById(sessionCreateDTO.getQuestionList());
        if (questionList == null) {
            return ResponseEntity.notFound().build();
        }

        if (!(sessionCreateDTO.getDate().matches("\\d\\d/\\d\\d/\\d\\d\\d\\d") && (sessionCreateDTO.getTime().matches("\\d\\d.\\d\\d")))) {
            return ResponseEntity.badRequest().build();
        }

        Session newSession = sessionService.createSession(new Session(sessionCreateDTO.getClassName(), admin, questionList, sessionCreateDTO.getDate(), sessionCreateDTO.getTime()));
        return new ResponseEntity<Session>(newSession, HttpStatus.OK);
    }

    @GetMapping(value="/admin/session/admin/{id}")
    public ResponseEntity<Set<Session>> getSessionsByAdmin(@PathVariable UUID id) {
        Admin admin = adminService.findById(id);
        if (admin == null) {
            return ResponseEntity.notFound().build();
        }
        Set<Session> sessions = admin.getSessions();
        return ResponseEntity.ok(sessions);
    }

    @PostMapping(value="/join/")
    public ResponseEntity<Player> joinSession(@RequestBody SessionJoinDTO sessionJoinDTO) {
        Session session = sessionService.findByCode(sessionJoinDTO.getCode());
        if (session == null) {
            return ResponseEntity.notFound().build();
        }
        if (!(session.getState().equals("Active"))){
            return new ResponseEntity("Unable to join inactive Session", HttpStatus.NOT_FOUND);
        }
        Player newPlayer = new Player(sessionJoinDTO.getName(), session);
        return ResponseEntity.ok(playerService.create(newPlayer));
    }

    @GetMapping(value = "/session/{sessionCode}")
    public ResponseEntity<QuestionList> getQuestionsByCode(@PathVariable String sessionCode) {
        Session session = sessionService.findByCode(sessionCode);
        if (session == null) {
            return ResponseEntity.notFound().build();
        }
        QuestionList questionList = session.getQuestionList();
        List<Question> questions = questionList.getQuestions();
        questions.sort(new Comparator<Question>() {
            @Override
            public int compare(Question o1, Question o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        questionList.setQuestions(questions);
        return ResponseEntity.ok(questionList);
    }

    @PutMapping(value = "/admin/launchSession/{sessionCode}")
    public ResponseEntity<Session> launchSession(@PathVariable String sessionCode) {
        Session session = sessionService.findByCode(sessionCode);
        if (session==null) {
            return ResponseEntity.notFound().build();
        }
        session.setState("Active");
        sessionService.createSession(session);
        return new ResponseEntity<Session>(session, HttpStatus.OK);
    }

    @PutMapping(value="/admin/endSession/{sessionCode}")
    public ResponseEntity<Session> endSession(@PathVariable String sessionCode) {
        Session session = sessionService.findByCode(sessionCode);
        if (session==null) {
            return ResponseEntity.notFound().build();
        }
        session.setState("Inactive");
        sessionService.createSession(session);
        return new ResponseEntity<Session>(session, HttpStatus.OK);
    }

    @DeleteMapping(value = "/admin/session/{sessionCode}")
    public ResponseEntity<Session> deleteSession(@PathVariable String sessionCode) {
        Session session = sessionService.findByCode(sessionCode);
        if (session==null) {
            return ResponseEntity.notFound().build();
        }
        sessionService.deleteSession(session);
        return new ResponseEntity<Session>(session, HttpStatus.OK);
    }
}
