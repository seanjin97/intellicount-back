package com.intellicount.model.ProfileGoals;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Profile.Profile;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class ProfileGoal implements Serializable {
    @EmbeddedId
    private ProfileGoalKey id;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("profileId")
    @JoinColumn
    @JsonBackReference
    private Profile profile;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("goalId")
    @JoinColumn
    @JsonManagedReference
    private Goal goal;

    double value;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public ProfileGoal() {
    }

    public ProfileGoal(Profile profile, Goal goal, double value) {
        this.id = new ProfileGoalKey(profile.getId(), goal.getId());
        this.profile = profile;
        this.goal = goal;
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
