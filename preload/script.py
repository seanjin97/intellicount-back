import json
import requests
import traceback
import sys

try:
    url = input("Please enter the base url: \ne.g http://localhost:8080\nURL: ")
    url = url.strip()
    key = None
    if (url != 'http://localhost:8080'):
        key = input("\n*** Data has already been preloaded, are you sure you want to load duplicate data into the DB? Things may break ***\n\nType Y to proceed\nType N to retrieve details without creating new rows\n")
        if (key=="N"):
            result = requests.post(url + "/login/", json={
            "name": "admin",
            "password": "admin"
            })

            admin = result.json()

            qLs = requests.get(url + '/quiz/admin/{id}'.format(id=admin)).json()
            ql = [i for i in qLs if i['type'] == 'DEFAULT'][0]

            session = requests.get(url + "/session/admin/{id}".format(id=admin)).json()
            session = [i for i in session if i['state'] == 'Active']
            if (len(session) == 0):
                session = {}
                session['id'] = "No active sessions"
                session['code'] = "No active sessions"
            else:
                session = session[0]
            print()
            print("CREDENTIALS")
            print("name: admin")
            print("password: admin")
            print("admin id:", admin)
            print("questionList id:", ql['id'])
            print("session code:", session['code'])
            print('session id:', session['id'])
            input('Press ENTER to exit')
            sys.exit()
        if (key != 'N' and key != 'Y'):
            print('idk what you talking bro')
            sys.exit()
    # load quiz data
    with open('./generated/ScenariosGenerated.json') as f:
        data = json.load(f)
    requests.post(url + "/scenario/", json=data)

    # load profile data
    with open('./generated/profiles.json') as f:
        data = json.load(f)
    requests.post(url + "/profile/", json=data)

    # load outcome data
    with open('./generated/outcomes.json') as f:
        data = json.load(f)
    requests.post(url + "/outcome/", json=data)

    # load default quiz data
    with open('./generated/QuizGenerated.json') as f:
        data = json.load(f)
    requests.post(url + "/quiz/dev/", json=data)

    # create and login
    requests.post(url + "/admin/", json={
        "name": "admin",
        "password": "admin"
    })

    result = requests.post(url + "/login/", json={
        "name": "admin",
        "password": "admin"
    })

    admin = result.json()
    # retrieve question list id
    questionList = requests.get(
        url + "/quiz/admin/{admin}".format(admin=admin)).json()[0]["id"]

    # create new session
    result = requests.post(
        url + "/session/", json={"admin": admin, "className": "class 3a", "questionList": questionList, "date": "03/03/2021", "time": "15.00"})
    # retrieve session code
    sessionCode = result.json()["code"]

    # create stopped session
    result = requests.post(
        url + "/session/", json={"admin": admin, "className": "class 3b", "questionList": questionList, "date": "04/03/2021", "time": "08.00"})
    # retrieve session code
    sessionCode = result.json()["code"]

    # end session
    result = requests.put(
        url + "/endSession/{code}".format(code=sessionCode))

    # create a new session
    result = requests.post(
        url + "/session/", json={"admin": admin, "className": "class 3c", "questionList": questionList, "date": "05/03/2021", "time": "14.00"})
    # retrieve session code
    sessionCode = result.json()["code"]
    sessionId = result.json()['id']

    # launch session
    result = requests.put(
        url + "/launchSession/{code}".format(code=sessionCode))

    print()
    print("Scenarios, Profiles, Outcomes, Questions loaded")
    print("CREDENTIALS")
    print("name: admin")
    print("password: admin")
    print("admin id:", admin)
    print("questionList id:", questionList)
    print("session code:", sessionCode)
    print('session id:', sessionId)
except Exception as e:
    print("Something went wrong")
    traceback.print_exc()
input('Press ENTER to exit')
