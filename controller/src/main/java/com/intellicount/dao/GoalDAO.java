package com.intellicount.dao;

import com.intellicount.model.Goal.Goal;

import java.util.UUID;

public interface GoalDAO {
    Goal findTopByName(String name);
    Goal create(Goal goal);
}
