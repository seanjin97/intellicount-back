package com.intellicount.model.Report;

import com.intellicount.model.Question.Question;
import com.intellicount.model.Question.QuestionResult;

import java.io.Serializable;
import java.util.List;

public class QuestionOverallView implements Serializable {
    List<QuestionResult> questionResults;

    public QuestionOverallView() {
    }

    public QuestionOverallView(List<QuestionResult> questionResults) {
        this.questionResults = questionResults;
    }

    public List<QuestionResult> getQuestionResults() {
        return questionResults;
    }

    public void setQuestionResults(List<QuestionResult> questionResults) {
        this.questionResults = questionResults;
    }
}
