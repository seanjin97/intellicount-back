package com.intellicount.dao;

import com.intellicount.model.Session.Session;
import com.intellicount.repositories.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("postgresSession")
public class PostgresSessionDataAccessService implements SessionDAO {
    private final SessionRepository sessionRepository;

    @Autowired
    public PostgresSessionDataAccessService(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Override
    public Session createSession(Session session) {
        return sessionRepository.save(session);
    }

    @Override
    public Session findByCode(String code) {
        return sessionRepository.findTopByCode(code);
    }

    @Override
    public Session findById(UUID id) {
        return sessionRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(Session session) {
        sessionRepository.delete(session);
    }
}
