package com.intellicount.dao;

import com.intellicount.model.QuestionList.QuestionList;
import com.intellicount.repositories.QuestionListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository("postgresQuestionList")
public class PostgresQuestionListDataAccessService implements QuestionListDAO{
    private final QuestionListRepository questionListRepository;

    @Autowired
    public PostgresQuestionListDataAccessService(QuestionListRepository questionListRepository) {
        this.questionListRepository = questionListRepository;
    }

    @Override
    public QuestionList findById(UUID id) {
        return questionListRepository.findById(id).orElse(null);
    }

    @Override
    public QuestionList create(QuestionList questionList) {
        return questionListRepository.save(questionList);
    }

    @Override
    public Set<QuestionList> findAllByType(String type) {
        return questionListRepository.findAllByType(type);
    }

    @Override
    public void delete(QuestionList questionList) {
        questionListRepository.delete(questionList);
    }
}
