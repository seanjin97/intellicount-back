import json
import requests
import traceback
import sys

url = "http://localhost:8080"

try:

    # load quiz data
    with open('./generated/ScenariosGenerated.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/scenario/", json=data)

    # load profile data
    with open('./generated/profiles.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/profile/", json=data)

    # load outcome data
    with open('./generated/outcomes.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/outcome/", json=data)

    # load default quiz data
    with open('./generated/QuizGenerated.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/quiz/", json=data)

    admin = 'f8dc8c53-ff5d-44b3-b929-e07c56a5634f'
    # sync admin with keycloak
    requests.post(url + "/keycloakUser/",
                  json={"id": admin, "name": "user1"})

    # retrieve question list
    questionList = requests.get(
        url + "/quiz/admin/{admin}".format(admin=admin)).json()[0]["id"]

   # create new session
    result = requests.post(
        url + "/admin/session/", json={"admin": admin, "className": "class 3a", "questionList": questionList, "date": "03/03/2021", "time": "15.00"})
    # retrieve session code
    sessionCode = result.json()["code"]

    # create stopped session
    result = requests.post(
        url + "/admin/session/", json={"admin": admin, "className": "class 3b", "questionList": questionList, "date": "04/03/2021", "time": "08.00"})
    # retrieve session code
    sessionCode = result.json()["code"]

    # end session
    result = requests.put(
        url + "/admin/endSession/{code}".format(code=sessionCode))

    # create a new session
    result = requests.post(
        url + "/admin/session/", json={"admin": admin, "className": "class 3c", "questionList": questionList, "date": "05/03/2021", "time": "14.00"})
    # retrieve session code
    sessionCode = result.json()["code"]
    sessionId = result.json()['id']

    # launch session
    result = requests.put(
        url + "/admin/launchSession/{code}".format(code=sessionCode))

    print()
    print("admin id:", admin)
    print("questionList id:", questionList)
    print("session code:", sessionCode)
    print('session id:', sessionId)
except Exception as e:
    print("Something went wrong")
    traceback.print_exc()
input('Press ENTER to exit')
