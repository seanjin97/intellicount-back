package com.intellicount.model.QuestionList;

import com.intellicount.model.Question.QuestionDTO;

import java.util.Set;
import java.util.UUID;

public class QuestionListDevDTO {
    private String name;
    private Set<QuestionDTO> questions;

    public QuestionListDevDTO() {
    }

    public Set<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<QuestionDTO> questions) {
        this.questions = questions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
