package com.intellicount.model.Report;

import com.intellicount.model.Player.PlayerResult;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SessionOverallView implements Serializable {
    private Map<Integer, Integer> sessionPercentiles = new HashMap<>();
    private Set<PlayerResult> playerResults = new HashSet<>();
    private double average;
    private double standardDeviation;

    public SessionOverallView() {
    }

    public SessionOverallView(Map<Integer, Integer> sessionPercentiles, Set<PlayerResult> playerResults, double average, double standardDeviation) {
        this.sessionPercentiles = sessionPercentiles;
        this.playerResults = playerResults;
        this.average = average;
        this.standardDeviation = standardDeviation;
    }

    public Map<Integer, Integer> getSessionPercentiles() {
        return sessionPercentiles;
    }

    public void setSessionPercentiles(Map<Integer, Integer> sessionPercentiles) {
        this.sessionPercentiles = sessionPercentiles;
    }

    public Set<PlayerResult> getPlayerResults() {
        return playerResults;
    }

    public void setPlayerResults(Set<PlayerResult> playerResults) {
        this.playerResults = playerResults;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public double getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(double standardDeviation) {
        this.standardDeviation = standardDeviation;
    }
}
