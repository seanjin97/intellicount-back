package com.intellicount.services;

import com.intellicount.dao.QuestionDAO;
import com.intellicount.model.Question.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class QuestionService {
    private final QuestionDAO questionDAO;

    @Autowired
    public QuestionService(@Qualifier("postgresQuestion") QuestionDAO questionDAO) {
        this.questionDAO = questionDAO;
    }

    public Question findById(UUID id) {
        return questionDAO.findById(id);
    }
}
