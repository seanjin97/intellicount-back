package com.intellicount.repositories;

import com.intellicount.model.Outcome.Outcome;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OutcomeRepository extends JpaRepository<Outcome, UUID> {
    Outcome findTopByName(String name);
}
