package com.intellicount.controllers;

import com.intellicount.model.Syllabus.Syllabus;
import com.intellicount.services.SyllabusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SyllabusController {
    private final SyllabusService syllabusService;

    @Autowired
    public SyllabusController(SyllabusService syllabusService) {
        this.syllabusService = syllabusService;
    }

    @GetMapping(value = "/syllabus/")
    public ResponseEntity<List<Syllabus>> getAllSyllabus() {
        return new ResponseEntity<List<Syllabus>>(syllabusService.getAll(), HttpStatus.OK);
    }
}
