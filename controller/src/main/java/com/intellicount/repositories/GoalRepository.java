package com.intellicount.repositories;

import com.intellicount.model.Goal.Goal;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface GoalRepository extends CrudRepository<Goal, UUID> {
    Goal findTopByName(String name);
}