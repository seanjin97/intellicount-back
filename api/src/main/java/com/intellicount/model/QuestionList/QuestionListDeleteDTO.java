package com.intellicount.model.QuestionList;

import java.io.Serializable;
import java.util.UUID;

public class QuestionListDeleteDTO implements Serializable {
    private UUID admin;
    private UUID questionList;

    public QuestionListDeleteDTO() {
    }

    public UUID getAdmin() {
        return admin;
    }

    public void setAdmin(UUID admin) {
        this.admin = admin;
    }

    public UUID getQuestionList() {
        return questionList;
    }

    public void setQuestionList(UUID questionList) {
        this.questionList = questionList;
    }
}
