package com.intellicount.services;

import com.intellicount.dao.ReportDAO;
import com.intellicount.model.Player.Player;
import com.intellicount.model.Report.Report;
import com.intellicount.model.Session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ReportService {
    private final ReportDAO reportDAO;

    @Autowired
    public ReportService(@Qualifier("postgresReport") ReportDAO reportDAO) {
        this.reportDAO = reportDAO;
    }

    public Report create(Report report) {
        return reportDAO.create(report);
    }

    public List<Report> retrieveBySession(Session session) {
        return reportDAO.retrieveBySession(session);
    }

    public Report findByPlayer(Player player) {
        return reportDAO.findByPlayer(player);
    }
}
