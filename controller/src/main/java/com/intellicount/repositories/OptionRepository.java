package com.intellicount.repositories;

import com.intellicount.model.Option.Option;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface OptionRepository extends CrudRepository<Option, UUID> {
}
