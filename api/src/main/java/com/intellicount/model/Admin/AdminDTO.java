package com.intellicount.model.Admin;

import com.intellicount.model.DTO;

import java.io.Serializable;
import java.util.UUID;

public class AdminDTO implements Serializable, DTO {
    private UUID id;
    private String name;

    public AdminDTO() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Admin toTrueClass() {
        return new Admin(id, name);
    }
}
