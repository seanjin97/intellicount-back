package com.intellicount.model.Decision;

import com.intellicount.model.DTO;
import com.intellicount.model.Scenario.Scenario;
import com.intellicount.model.Scenario.ScenarioDTO;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class DecisionDTO implements Serializable {

    @NotBlank
    private String text;
    private double cash;
    private double efficiency;
    private double reputation;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(double efficiency) {
        this.efficiency = efficiency;
    }

    public double getReputation() {
        return reputation;
    }

    public void setReputation(double reputation) {
        this.reputation = reputation;
    }

    public Decision toTrueClass(Scenario scenario) {
        return new Decision(
                this.text, this.cash, this.efficiency, this.reputation, scenario
        );
    }

}
