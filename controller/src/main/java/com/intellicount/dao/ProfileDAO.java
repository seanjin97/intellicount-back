package com.intellicount.dao;

import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Profile.Profile;

import java.util.List;
import java.util.UUID;

public interface ProfileDAO {

    Profile findById(UUID id);

    List<Profile> getAll();

    void create(Profile profile);

    void createGoal(Goal goal);

    Profile findTopByProfile(String profile);
}
