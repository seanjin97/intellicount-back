package com.intellicount.dao;

import com.intellicount.model.Goal.Goal;
import com.intellicount.repositories.GoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("postgresGoal")
public class PostgresGoalDataAccessService implements GoalDAO{
    private final GoalRepository goalRepository;

    @Autowired
    public PostgresGoalDataAccessService(GoalRepository goalRepository) {
        this.goalRepository = goalRepository;
    }

    @Override
    public Goal findTopByName(String name) {
        return goalRepository.findTopByName(name);
    }

    @Override
    public Goal create(Goal goal) {
        return goalRepository.save(goal);
    }
}
