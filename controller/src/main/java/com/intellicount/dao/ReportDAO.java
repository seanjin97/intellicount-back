package com.intellicount.dao;

import com.intellicount.model.Player.Player;
import com.intellicount.model.Report.Report;
import com.intellicount.model.Session.Session;

import java.util.List;
import java.util.UUID;

public interface ReportDAO {
    Report create(Report report);

    List<Report> retrieveBySession(Session session);

    Report findByPlayer(Player player);
}
