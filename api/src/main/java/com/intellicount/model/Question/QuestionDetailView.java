package com.intellicount.model.Question;

import com.intellicount.model.Option.OptionResult;
import com.intellicount.model.Syllabus.Syllabus;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class QuestionDetailView implements Serializable {
    private String questionText;
    private String difficulty;
    private Map<String, OptionResult> optionResult = new HashMap<>();
    private Syllabus syllabus;
    private int numAttempted;

    public QuestionDetailView() {
    }

    public QuestionDetailView(String questionText, String difficulty, Map<String, OptionResult> optionResult, Syllabus syllabus, int numAttempted) {
        this.questionText = questionText;
        this.difficulty = difficulty;
        this.optionResult = optionResult;
        this.syllabus = syllabus;
        this.numAttempted = numAttempted;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public Map<String, OptionResult> getOptionResult() {
        return optionResult;
    }

    public void setOptionResult(Map<String, OptionResult> optionResult) {
        this.optionResult = optionResult;
    }

    public Syllabus getSyllabus() {
        return syllabus;
    }

    public void setSyllabus(Syllabus syllabus) {
        this.syllabus = syllabus;
    }

    public int getNumAttempted() {
        return numAttempted;
    }

    public void setNumAttempted(int numAttempted) {
        this.numAttempted = numAttempted;
    }
}
