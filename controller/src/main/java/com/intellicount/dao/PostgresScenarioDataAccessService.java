package com.intellicount.dao;


import com.intellicount.model.Decision.Decision;
import com.intellicount.model.Scenario.Scenario;
import com.intellicount.repositories.DecisionRepository;
import com.intellicount.repositories.ScenarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;


@Repository("postgresScenario")
public class PostgresScenarioDataAccessService implements ScenarioDAO {

    private final ScenarioRepository scenarioRepository;
    private final DecisionRepository decisionRepository;
    @Autowired
    public PostgresScenarioDataAccessService(ScenarioRepository scenarioRepository, DecisionRepository decisionRepository){
        this.scenarioRepository = scenarioRepository;
        this.decisionRepository = decisionRepository;
    }

    @Override
    public Scenario findById(UUID id) {
        return scenarioRepository.findById(id).orElse(null);
    }

    @Override
    public List<Scenario> getAll() {
        return scenarioRepository.findAll();
    }

    @Override
    public void create(Scenario scenario) {
        scenarioRepository.save(scenario);
    }

    @Override
    public void update(Scenario updatedScenario) {
        scenarioRepository.save(updatedScenario);
    }

    @Override
    public void delete(Scenario scenario) {
        scenarioRepository.delete(scenario);
    }

    @Override
    public void createDecision(Decision decision){
        decisionRepository.save(decision);
    }

    @Override
    public Scenario findTopByQuestion(String question) {
        return scenarioRepository.findTopByQuestion(question);
    }


}
