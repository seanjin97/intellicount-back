package com.intellicount.repositories;

import com.intellicount.model.Answer.Answer;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface AnswerRepository extends CrudRepository<Answer, UUID> {
}
