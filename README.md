Swagger-UI
> http://localhost:8080/swagger-ui/

Some notes:
1. Packages:
   > controller: contains the API endpoints, passed to the methods in the service layer <br>
   > service: contains the methods used to interact with the DAO layer<br>
   > DAO: contains the methods that utilises JPA syntax to interact with the DB
   > repository: link Hibernate with the H2/ PostgresDB <br>
