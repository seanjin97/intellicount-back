package com.intellicount.model.Answer;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class AnswerSubmitDTO implements Serializable {
    private UUID player;
    private int totalScore;
    private List<SingleAnswerSubmitDTO> answers;

    public AnswerSubmitDTO() {
    }

    public List<SingleAnswerSubmitDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<SingleAnswerSubmitDTO> answers) {
        this.answers = answers;
    }

    public UUID getPlayer() {
        return player;
    }

    public void setPlayer(UUID player) {
        this.player = player;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }
}
