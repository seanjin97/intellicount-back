package com.intellicount.dao;

import com.intellicount.model.Admin.Admin;
import com.intellicount.repositories.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository("postgresAdmin")
public class PostgresAdminDataAccessService implements AdminDAO{
    private final AdminRepository adminRepository;

    @Autowired
    public PostgresAdminDataAccessService(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    public Admin findByID(UUID id) {
        return adminRepository.findById(id).orElse(null);
    }
    public Admin create(Admin admin) {
        return adminRepository.save(admin);
    }
    public Admin findByName(String name) {
        return adminRepository.findTopByName(name);
    }
}
