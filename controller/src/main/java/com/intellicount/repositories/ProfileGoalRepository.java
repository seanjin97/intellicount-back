package com.intellicount.repositories;

import com.intellicount.model.ProfileGoals.ProfileGoal;
import com.intellicount.model.ProfileGoals.ProfileGoalKey;
import org.springframework.data.repository.CrudRepository;

public interface ProfileGoalRepository extends CrudRepository<ProfileGoal, ProfileGoalKey> {
}
