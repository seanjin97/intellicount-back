package com.intellicount.services;

import com.intellicount.dao.ProfileDAO;
import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Profile.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProfileService {

    private final ProfileDAO profileDAO;

    @Autowired
    public ProfileService(@Qualifier("postgresProfile") ProfileDAO profileDAO) {
        this.profileDAO = profileDAO;
    }

    public Profile findById(UUID id) {
        return profileDAO.findById(id);
    }

    public List<Profile> getAll() {
        return profileDAO.getAll();
    }

    public void create(Profile profile){
        profileDAO.create(profile);
    }

    public Profile findTopByProfile(String profile) {
        return profileDAO.findTopByProfile(profile);
    }
}
