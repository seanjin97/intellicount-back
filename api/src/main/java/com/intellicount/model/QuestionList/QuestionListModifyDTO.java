package com.intellicount.model.QuestionList;

import com.intellicount.model.Question.QuestionDTO;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

public class QuestionListModifyDTO implements Serializable {
    private UUID admin;
    private UUID questionList;
    private String name;
    private Set<QuestionDTO> questions;

    public QuestionListModifyDTO() {
    }

    public UUID getAdmin() {
        return admin;
    }

    public void setAdmin(UUID admin) {
        this.admin = admin;
    }

    public UUID getQuestionList() {
        return questionList;
    }

    public void setQuestionList(UUID questionList) {
        this.questionList = questionList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<QuestionDTO> questions) {
        this.questions = questions;
    }
}
