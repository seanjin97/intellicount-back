package com.intellicount.services;

import com.intellicount.dao.OptionDAO;
import com.intellicount.model.Option.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class OptionService {
    private final OptionDAO optionDAO;

    @Autowired
    public OptionService(OptionDAO optionDAO) {
        this.optionDAO = optionDAO;
    }

    public Option findById(UUID id) {
        return optionDAO.findById(id);
    }

    public Option create(Option option) {
        return optionDAO.create(option);
    }
}
