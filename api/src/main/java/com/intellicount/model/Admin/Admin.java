package com.intellicount.model.Admin;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.intellicount.model.QuestionList.QuestionList;
import com.intellicount.model.Session.Session;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
public class Admin implements Serializable {
    @Id
//    @GeneratedValue
    private UUID id;
    private String name;

//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
//    private String password;

    @OneToMany(mappedBy = "admin", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Session> sessions;

    //    @OneToMany(mappedBy="admin", fetch = FetchType.LAZY, cascade= CascadeType.ALL)
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable
    @JsonManagedReference
    private Set<QuestionList> questionLists = new HashSet<>();

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Admin() {
    }

    public Admin(UUID id, String name) {
        this.name = name;
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Session> getSessions() {
        return sessions;
    }

    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }

//    public Organisation getOrganisation() {
//        return organisation;
//    }
//
//    public void setOrganisation(Organisation organisation) {
//        this.organisation = organisation;
//    }

    public Set<QuestionList> getQuestionLists() {
        return questionLists;
    }

    public void setQuestionLists(Set<QuestionList> questionLists) {
        this.questionLists = questionLists;
    }

    public void addQuestionList(QuestionList questionList) {
        this.questionLists.add(questionList);
        Set<Admin> retrieveAdmins = questionList.getAdmins();
        retrieveAdmins.add(this);
        questionList.setAdmins(retrieveAdmins);
    }

    public void removeQuestionList(QuestionList questionList) {
        this.questionLists.remove(questionList);
        Set<Admin> retrieveAdmins = questionList.getAdmins();
        retrieveAdmins.remove(this);
        questionList.setAdmins(retrieveAdmins);
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }
}
