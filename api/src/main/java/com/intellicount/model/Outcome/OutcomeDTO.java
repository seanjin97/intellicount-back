package com.intellicount.model.Outcome;

import com.intellicount.model.DTO;
import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Goal.GoalDTO;
import com.intellicount.model.Goal.GoalOutcomeDTO;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

public class OutcomeDTO implements Serializable {

    @NotBlank
    private String name;

    @NotBlank
    private String financialResult;

    @NotBlank
    private String success;

    @NotBlank
    private String failure;

    @NotBlank
    private GoalOutcomeDTO goal;

    public OutcomeDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFinancialResult() {
        return financialResult;
    }

    public void setFinancialResult(String financialResult) {
        this.financialResult = financialResult;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getFailure() {
        return failure;
    }

    public void setFailure(String failure) {
        this.failure = failure;
    }

    public GoalOutcomeDTO getGoal() {
        return goal;
    }

    public void setGoal(GoalOutcomeDTO goal) {
        this.goal = goal;
    }

    public Outcome toTrueClass(Goal goal) {
        return new Outcome(this.name, this.financialResult,this.success, this.failure, goal);
    }
}
