package com.intellicount.model.Session;

import com.fasterxml.jackson.annotation.*;
import com.intellicount.model.Admin.Admin;
import com.intellicount.model.Player.Player;
import com.intellicount.model.QuestionList.QuestionList;
import com.intellicount.model.Report.Report;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
public class Session implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;
    private String className;
    private String code;
    private String state;
    private String date;
    private String time;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(nullable = false)
    @JsonBackReference
    private Admin admin;

    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonManagedReference
    private QuestionList questionList;

    @OneToMany(mappedBy = "session", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Player> players = new HashSet<>();

    @OneToMany(mappedBy = "session", fetch = FetchType.LAZY)
    @JsonBackReference
    private Set<Report> reports;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Session() {
    }

    public Session(String className, Admin admin, QuestionList questionList, String date, String time) {
        this.className = className;
        this.code = RandomStringUtils.randomAlphanumeric(6);
        this.admin = admin;
        this.questionList = questionList;
        this.state = "New";
        this.date = date;
        this.time = time;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public QuestionList getQuestionList() {
        return this.questionList;
    }

    public void setQuestionList(QuestionList questionList) {
        this.questionList = questionList;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Set<Report> getReports() {
        return reports;
    }

    public void setReports(Set<Report> reports) {
        this.reports = reports;
    }
}
