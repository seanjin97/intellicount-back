package com.intellicount.dao;

import com.intellicount.model.Question.Question;
import com.intellicount.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("postgresQuestion")
public class PostgresQuestionDataAccessService implements QuestionDAO{
    private final QuestionRepository questionRepository;

    @Autowired
    public PostgresQuestionDataAccessService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public Question findById(UUID id) {
        return questionRepository.findById(id).orElse(null);
    }
}
