import json
import requests
import traceback
import sys

auth = "https://keycloak.intellicount-auth.com/auth/realms/intellicount/protocol/openid-connect/token"
url = "http://localhost:8080"

try:
    username = input("username: ")
    password = input("password: ")
    data = {
        "client_id": "IntellicountAuth",
        "username": username,
        "password": password,
        "grant_type": "password"
    }

    token = requests.post(auth, data=data).json()['access_token']

    headers = {"Authorization": "Bearer " + token}
    # load quiz data
    with open('./generated/ScenariosGenerated.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/scenario/", json=data, headers=headers)

    # load profile data
    with open('./generated/profiles.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/profile/", json=data, headers=headers)

    # load outcome data
    with open('./generated/outcomes.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/outcome/", json=data, headers=headers)

    # load default quiz data
    with open('./generated/QuizGenerated.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/quiz/", json=data, headers=headers)

    print()
    print('preloaded')
except Exception as e:
    print("Something went wrong")
    traceback.print_exc()
input('Press ENTER to exit')
