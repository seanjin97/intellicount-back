package com.intellicount.services;

import com.intellicount.dao.QuestionListDAO;
import com.intellicount.model.Question.Question;
import com.intellicount.model.QuestionList.QuestionList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.UUID;

@Service
public class QuestionListService {
    private final QuestionListDAO questionListDAO;

    @Autowired
    public QuestionListService(@Qualifier("postgresQuestionList") QuestionListDAO questionListDAO) {
        this.questionListDAO = questionListDAO;
    }

    public QuestionList findById(UUID id) {
        return questionListDAO.findById(id);
    }

    public QuestionList create(QuestionList newQuestionList) {
        return questionListDAO.create(newQuestionList);
    }

    public Set<QuestionList> findAllByType(String type) {
        return questionListDAO.findAllByType(type);
    }

    public void delete(QuestionList questionList) {
        questionListDAO.delete(questionList);
    }
}
