import json
import requests
import traceback
import sys

auth = "https://keycloak.intellicount-auth.com/auth/realms/intellicount/protocol/openid-connect/token"
url = "http://localhost:8080"

try:
    username = input("username: ")
    password = input("password: ")
    data = {
        "client_id": "IntellicountAuth",
        "username": username,
        "password": password,
        "grant_type": "password"
    }

    token = requests.post(auth, data=data).json()['access_token']

    headers = {"Authorization": "Bearer " + token}
    # load quiz data
    with open('./generated/ScenariosGenerated.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/scenario/", json=data, headers=headers)

    # load profile data
    with open('./generated/profiles.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/profile/", json=data, headers=headers)

    # load outcome data
    with open('./generated/outcomes.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/outcome/", json=data, headers=headers)

    # load default quiz data
    with open('./generated/QuizGenerated.json') as f:
        data = json.load(f)
    requests.post(url + "/dev/quiz/", json=data, headers=headers)

    admin = 'c1c9edb2-71ca-41f5-9f36-fd7382b0a043'
    # sync admin with keycloak
    requests.post(url + "/keycloakUser/",
                  json={"id": admin, "name": "user1"}, headers=headers)

    # retrieve question list
    questionList = requests.get(
        url + "/quiz/admin/{admin}".format(admin=admin), headers=headers).json()[0]["id"]

   # create new session
    result = requests.post(
        url + "/admin/session/", json={"admin": admin, "className": "class 3a", "questionList": questionList, "date": "03/03/2021", "time": "15.00"}, headers=headers)
    # retrieve session code
    sessionCode = result.json()["code"]

    # create stopped session
    result = requests.post(
        url + "/admin/session/", json={"admin": admin, "className": "class 3b", "questionList": questionList, "date": "04/03/2021", "time": "08.00"}, headers=headers)
    # retrieve session code
    sessionCode = result.json()["code"]

    # end session
    result = requests.put(
        url + "/admin/endSession/{code}".format(code=sessionCode), headers=headers)

    # create a new session
    result = requests.post(
        url + "/admin/session/", json={"admin": admin, "className": "class 3c", "questionList": questionList, "date": "05/03/2021", "time": "14.00"}, headers=headers)
    # retrieve session code
    sessionCode = result.json()["code"]
    sessionId = result.json()['id']

    # launch session
    result = requests.put(
        url + "/admin/launchSession/{code}".format(code=sessionCode), headers=headers)

    print()
    print("admin id:", admin)
    print("questionList id:", questionList)
    print("session code:", sessionCode)
    print('session id:', sessionId)
except Exception as e:
    print("Something went wrong")
    traceback.print_exc()
input('Press ENTER to exit')
