package com.intellicount.model.Answer;

import com.fasterxml.jackson.annotation.*;
import com.intellicount.model.Option.Option;
import com.intellicount.model.Player.Player;
import com.intellicount.model.Question.Question;
import com.intellicount.model.Report.Report;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = UUID.class)
public class Answer implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @JsonManagedReference
    private Question question;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn
    @JsonManagedReference
    private Option option;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    @JsonIgnore
    private Player player;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    @JsonBackReference
    private Report report;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Answer() {
    }

    public Answer(Question question, Option option, Player player, Report report) {
        this.question = question;
        this.option = option;
        this.player = player;
        this.report = report;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }
}
