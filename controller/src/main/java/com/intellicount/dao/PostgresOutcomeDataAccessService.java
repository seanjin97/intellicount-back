package com.intellicount.dao;

import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Outcome.Outcome;
import com.intellicount.repositories.GoalRepository;
import com.intellicount.repositories.OutcomeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("postgresOutcome")
public class PostgresOutcomeDataAccessService implements OutcomeDAO{

    private final OutcomeRepository outcomeRepository;
    @Autowired
    public PostgresOutcomeDataAccessService(OutcomeRepository outcomeRepository){
        this.outcomeRepository = outcomeRepository;
    }

    @Override
    public Outcome findById(UUID id){
        return outcomeRepository.findById(id).orElse(null);
    }

    @Override
    public List<Outcome> getAll() {
        return outcomeRepository.findAll();
    }

    @Override
    public void create (Outcome outcome){
        outcomeRepository.save(outcome);
    }

    @Override
    public Outcome findByName(String name) {
        return outcomeRepository.findTopByName(name);
    }
}
