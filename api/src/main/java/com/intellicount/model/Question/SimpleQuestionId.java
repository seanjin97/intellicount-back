package com.intellicount.model.Question;

import java.util.UUID;

public class SimpleQuestionId {
    private int simpleId;
    private UUID id;

    public SimpleQuestionId(int simpleId, UUID id) {
        this.simpleId = simpleId;
        this.id = id;
    }

    public int getSimpleId() {
        return simpleId;
    }

    public void setSimpleId(int simpleId) {
        this.simpleId = simpleId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleQuestionId that = (SimpleQuestionId) o;

        if (simpleId != that.simpleId) return false;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        int result = simpleId;
        result = 31 * result + id.hashCode();
        return result;
    }
}
