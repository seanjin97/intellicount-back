package com.intellicount.dao;

import com.intellicount.model.Session.Session;

import java.util.List;
import java.util.UUID;

public interface SessionDAO {
    Session createSession(Session session);

    Session findByCode(String code);

    Session findById(UUID id);

    void delete (Session session);
}
