package com.intellicount.model.ProfileGoals;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Embeddable
public class ProfileGoalKey implements Serializable {
    private UUID profileId;
    private UUID goalId;

    public ProfileGoalKey() {
    }

    public ProfileGoalKey(UUID profileId, UUID goalId) {
        this.profileId = profileId;
        this.goalId = goalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProfileGoalKey that = (ProfileGoalKey) o;

        if (!profileId.equals(that.profileId)) return false;
        return goalId.equals(that.goalId);
    }

    @Override
    public int hashCode() {
        int result = profileId.hashCode();
        result = 31 * result + goalId.hashCode();
        return result;
    }
}
