package com.intellicount.model.Session;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.UUID;

public class SessionCreateDTO implements Serializable {
    @NotBlank
    private String className;
    @NotBlank
    private UUID admin;
    @NotBlank
    private UUID questionList;
    @NotBlank
    private String date;
    @NotBlank
    private String time;

    public SessionCreateDTO() {
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public UUID getAdmin() {
        return admin;
    }

    public void setAdmin(UUID admin) {
        this.admin = admin;
    }

    public UUID getQuestionList() {
        return questionList;
    }

    public void setQuestionList(UUID questionList) {
        this.questionList = questionList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
