package com.intellicount.repositories;

import com.intellicount.model.Scenario.Scenario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface ScenarioRepository extends JpaRepository<Scenario, UUID> {
    Scenario findTopByQuestion(String question);
}