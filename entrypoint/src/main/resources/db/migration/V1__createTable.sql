-- drop table if exists admin CASCADE;
-- drop table if exists answer CASCADE;
-- drop table if exists decision CASCADE;
-- drop table if exists goal CASCADE;
-- drop table if exists option CASCADE;
-- drop table if exists outcome CASCADE;
-- drop table if exists player CASCADE;
-- drop table if exists profile CASCADE;
-- drop table if exists profile_goals CASCADE;
-- drop table if exists question CASCADE;
-- drop table if exists question_list CASCADE;
-- drop table if exists report CASCADE;
-- drop table if exists scenario CASCADE;
-- drop table if exists session CASCADE;
-- drop table if exists syllabus CASCADE;
-- create table admin (id binary not null, name varchar(255), password varchar(255), primary key (id));
-- create table answer (id binary not null, option_id binary, player_id binary, question_id binary, report_id binary, primary key (id));
-- create table decision (id binary not null, cash double not null, efficiency double not null, reputation double not null, text varchar(255), scenario_id binary, primary key (id));
-- create table goal (id binary not null, name varchar(255), value double not null, primary key (id));
-- create table option (id binary not null, correct boolean not null, text varchar(255), question_id binary, primary key (id));
-- create table outcome (id binary not null, failure varchar(255), financial_result varchar(255), name varchar(255), success varchar(255), goal_id binary, primary key (id));
-- create table player (id binary not null, name varchar(255), streak integer not null, session_id binary, primary key (id));
-- create table profile (id binary not null, company varchar(255), initial_cost double not null, initial_revenue double not null, message varchar(255), profile varchar(255), primary key (id));
-- create table profile_goals (value double not null, goal_id binary not null, profile_id binary not null, primary key (goal_id, profile_id));
-- create table question (id binary not null, efficiency double not null, reputation double not null, result varchar(255), text varchar(255), question_list_id binary, syllabus_id binary, primary key (id));
-- create table question_list (id binary not null, name varchar(255), admin_id binary, primary key (id));
-- create table report (id binary not null, player_id binary, session_id binary, primary key (id));
-- create table scenario (id binary not null, question varchar(255), text varchar(255), primary key (id));
-- create table session (id binary not null, code varchar(255), name varchar(255), admin_id binary not null, question_list_id binary not null, primary key (id));
-- create table syllabus (id binary not null, chapter varchar(255), name varchar(255), primary key (id));
-- alter table answer add constraint FKjtpa8yh8oidkbnaiv88bvx4hy foreign key (option_id) references option;
-- alter table answer add constraint FKjksvfjygjpjo4j3ueaslaoqgc foreign key (player_id) references player;
-- alter table answer add constraint FK8frr4bcabmmeyyu60qt7iiblo foreign key (question_id) references question;
-- alter table answer add constraint FK8xdbl6buwvae87o7tpny15o3x foreign key (report_id) references report;
-- alter table decision add constraint FKrg70xo9h9xla7326rf2vksgfi foreign key (scenario_id) references scenario;
-- alter table option add constraint FKgtlhwmagte7l2ssfsgw47x9ka foreign key (question_id) references question;
-- alter table outcome add constraint FKnx95wm3l2lnv4mi68oyqfn8kf foreign key (goal_id) references goal;
-- alter table player add constraint FKfyq21u55953axrpxkv1q7bog0 foreign key (session_id) references session;
-- alter table profile_goals add constraint FKqgrwhw9svbehiuysbkhk899a3 foreign key (goal_id) references goal;
-- alter table profile_goals add constraint FKoufvoo6jf1qhmi3h9111tb6hg foreign key (profile_id) references profile;
-- alter table question add constraint FK4e7cev995soc70p3tfgbcc8fp foreign key (question_list_id) references question_list;
-- alter table question add constraint FKoja3auwxdrs1k22mj5i07mn4r foreign key (syllabus_id) references syllabus;
-- alter table question_list add constraint FKpq9ww57aymf17bmwjtp8wqkos foreign key (admin_id) references admin;
-- alter table report add constraint FKt9spg3rgvagyse04u4osrx0o5 foreign key (player_id) references player;
-- alter table report add constraint FKcnqbup65rhdc086v3wou3e5mc foreign key (session_id) references session;
-- alter table session add constraint FKalckk8ubqbptglmkgnb2rifm foreign key (admin_id) references admin;
-- alter table session add constraint FKl22b7cj66di9kmdr39w7n21h0 foreign key (question_list_id) references question_list