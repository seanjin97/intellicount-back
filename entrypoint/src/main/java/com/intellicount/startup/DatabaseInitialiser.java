package com.intellicount.startup;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellicount.model.Admin.Admin;
import com.intellicount.model.Decision.Decision;
import com.intellicount.model.Decision.DecisionDTO;
import com.intellicount.model.Scenario.Scenario;
import com.intellicount.model.Scenario.ScenarioDTO;
import com.intellicount.model.Scenario.ScenarioSetDTO;
import com.intellicount.repositories.DecisionRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@Profile("not-found")
public class DatabaseInitialiser implements ApplicationRunner{
    private final DecisionRepository decisionRepository;

    public DatabaseInitialiser(DecisionRepository decisionRepository) {
        this.decisionRepository = decisionRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        // load all scenarios and decisions
        List<Decision> decisionList = new ArrayList<>();
        try{
            ScenarioSetDTO scenarioSetDTO = mapper.readValue(new ClassPathResource("files/ScenariosGenerated.json").getFile(), ScenarioSetDTO.class);
            for (ScenarioDTO i : scenarioSetDTO.getScenarios()) {
                Scenario newScenario = i.toTrueClass();
                Set<DecisionDTO> decisionDTOs = i.getDecisions();
                Set<Decision> decisions = new HashSet<>();
                for (DecisionDTO decision : decisionDTOs) {
                    Decision newDecision = decision.toTrueClass(newScenario);
                    decisionList.add(newDecision);
                }
            }
            this.decisionRepository.saveAll(decisionList);
        }
        catch (IOException e){
            e.printStackTrace();
        }

        // create a new admin user
//        Admin admin = new Admin("admin", "admin");


//        List<>
    }
}
