package com.intellicount.model.Option;

import java.io.Serializable;

public class OptionResult implements Serializable {
    private int numCorrect;
    private boolean correct;
    private int numChosen;

    public OptionResult() {
    }

    public OptionResult(int numCorrect, boolean correct, int numChosen) {
        this.numCorrect = numCorrect;
        this.correct = correct;
        this.numChosen = numChosen;
    }

    public int getNumCorrect() {
        return numCorrect;
    }

    public void setNumCorrect(int numCorrect) {
        this.numCorrect = numCorrect;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public int getNumChosen() {
        return numChosen;
    }

    public void setNumChosen(int numChosen) {
        this.numChosen = numChosen;
    }
}
