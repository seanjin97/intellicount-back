package com.intellicount.services;

import com.intellicount.dao.SyllabusDAO;
import com.intellicount.model.Syllabus.Syllabus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SyllabusService {
    private final SyllabusDAO syllabusDAO;

    @Autowired
    public SyllabusService(SyllabusDAO syllabusDAO) {
        this.syllabusDAO = syllabusDAO;
    }

    public List<Syllabus> getAll() {
        return syllabusDAO.getAll();
    }

    public Syllabus findTopByChapterAndName(String chapter, String name) {
        return syllabusDAO.findByChapterAndName(chapter, name);
    }
}
