package com.intellicount.model.Scenario;

import com.intellicount.model.DTO;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

public class ScenarioSetDTO implements Serializable, DTO {
    private Set<ScenarioDTO> scenarios;

    public ScenarioSetDTO() {
    }

    public Set<ScenarioDTO> getScenarios() {
        return scenarios;
    }

    public void setScenarios(Set<ScenarioDTO> scenarios) {
        this.scenarios = scenarios;
    }

    public ScenarioDTO toTrueClass() {
        return new ScenarioDTO();
    }
}
