package com.intellicount.dao;

import com.intellicount.model.QuestionList.QuestionList;

import java.util.Set;
import java.util.UUID;

public interface QuestionListDAO {
    QuestionList findById(UUID id);
    QuestionList create(QuestionList questionList);
    Set<QuestionList> findAllByType(String type);
    void delete(QuestionList questionList);
}
