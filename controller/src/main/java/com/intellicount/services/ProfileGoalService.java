package com.intellicount.services;

import com.intellicount.dao.ProfileGoalDAO;
import com.intellicount.model.ProfileGoals.ProfileGoal;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ProfileGoalService {
    private final ProfileGoalDAO profileGoalDAO;

    public ProfileGoalService(@Qualifier("PostgresProfileGoal") ProfileGoalDAO profileGoalDAO) {
        this.profileGoalDAO = profileGoalDAO;
    }

    public ProfileGoal create(ProfileGoal profileGoal) {
        return profileGoalDAO.create(profileGoal);
    }
}
