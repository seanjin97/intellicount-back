package com.intellicount.model.Profile;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.intellicount.model.Goal.Goal;
import com.intellicount.model.ProfileGoals.ProfileGoal;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
public class Profile implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;
    private String profile;
    private String company;
    private String message;
    private double initialRevenue;
    private double initialCost;

    @OneToMany(mappedBy = "profile", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JsonManagedReference
    Set<ProfileGoal> profileGoals = new HashSet<>();

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Profile() {
    }

    public Profile(String profile, String company, String message, double initialRevenue, double initialCost) {
        this.profile = profile;
        this.company = company;
        this.message = message;
        this.initialRevenue = initialRevenue;
        this.initialCost = initialCost;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getInitialRevenue() {
        return initialRevenue;
    }

    public void setInitialRevenue(double initialRevenue) {
        this.initialRevenue = initialRevenue;
    }

    public double getInitialCost() {
        return initialCost;
    }

    public void setInitialCost(double initialCost) {
        this.initialCost = initialCost;
    }

    public Set<ProfileGoal> getProfileGoals() {
        return profileGoals;
    }

    public void setProfileGoals(Set<ProfileGoal> profileGoals) {
        this.profileGoals = profileGoals;
    }
}
