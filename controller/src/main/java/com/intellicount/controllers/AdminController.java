package com.intellicount.controllers;

import com.intellicount.model.Admin.Admin;
import com.intellicount.model.Admin.AdminDTO;
import com.intellicount.model.QuestionList.QuestionList;
import com.intellicount.services.AdminService;
import com.intellicount.services.QuestionListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.UUID;

@RestController
public class AdminController {
    private final AdminService adminService;
    private final QuestionListService questionListService;

    @Autowired
    public AdminController(AdminService adminService, QuestionListService questionListService) {
        this.adminService = adminService;
        this.questionListService = questionListService;
    }

//    @PostMapping(value = "/admin/")
//    public ResponseEntity<String> createAdmin(@RequestBody AdminDTO newAdmin){
//        if (adminService.findByUser(newAdmin.getName()) != null) {
//            return new ResponseEntity("Please choose another username", HttpStatus.BAD_REQUEST);
//        }
//        Admin admin = newAdmin.toTrueClass();
//        Set<QuestionList> defaultQuestions = questionListService.findAllByType("DEFAULT");
//
//        if (defaultQuestions.size() == 0) {
//            return new ResponseEntity("No DEFAULT QuestionLists found", HttpStatus.NOT_FOUND);
//        }
//        for (QuestionList i : defaultQuestions) {
//            i.addAdmin(admin);
//        }
//        adminService.create(admin);
//        return ResponseEntity.ok("CREATED");
//    }
//
//
//    @PostMapping(value = "/login/")
//    public ResponseEntity<UUID> loginAdmin(@RequestBody AdminDTO existingAdmin) {
//        Admin checkerAdmin = adminService.findByUser(existingAdmin.getName());
//        if (checkerAdmin == null) {
//            return ResponseEntity.notFound().build();
//        }
//        if (!(checkerAdmin.getPassword().equals(existingAdmin.getPassword()))) {
//            return ResponseEntity.badRequest().build();
//        }
//        return ResponseEntity.ok(checkerAdmin.getId());
//    }

    @PostMapping(value="/keycloakUser/")
    public ResponseEntity keycloakUser(@RequestBody AdminDTO adminKeycloakDTO) {
        UUID id = adminKeycloakDTO.getId();
        Admin admin = adminService.findById(id);
        if (admin == null) {
            admin = adminKeycloakDTO.toTrueClass();
        }
        Set<QuestionList> defaultQuestions = questionListService.findAllByType("DEFAULT");
        if (defaultQuestions.size() == 0) {
            return new ResponseEntity("No DEFAULT QuestionLists found", HttpStatus.NOT_FOUND);
        }
        for (QuestionList i : defaultQuestions) {
            i.addAdmin(admin);
        }
        adminService.create(admin);
        return ResponseEntity.ok(admin);
    }

}
