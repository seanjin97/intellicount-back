package com.intellicount.dao;

import com.intellicount.model.Outcome.Outcome;
import com.intellicount.model.Syllabus.Syllabus;
import com.intellicount.repositories.SyllabusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("postgresSyllabus")
public class PostgresSyllabusDataAccessService implements SyllabusDAO{
    private final SyllabusRepository syllabusRepository;

    @Autowired
    public PostgresSyllabusDataAccessService(SyllabusRepository syllabusRepository) {
        this.syllabusRepository = syllabusRepository;
    }

    @Override
    public Syllabus findByChapterAndName(String chapter, String name) {
        return syllabusRepository.findTopByChapterAndName(chapter, name);
    }

    @Override
    public List<Syllabus> getAll() {
        return syllabusRepository.findAll();
    }
}
