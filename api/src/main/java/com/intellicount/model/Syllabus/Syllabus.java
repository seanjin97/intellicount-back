package com.intellicount.model.Syllabus;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.intellicount.model.Question.Question;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Entity
public class Syllabus implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;
    private String chapter;
    private String name;

    @OneToMany(mappedBy = "syllabus", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonBackReference
    private Set<Question> questions;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Syllabus() {
    }

    public Syllabus(String chapter, String name) {
        this.chapter = chapter;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Syllabus syllabus = (Syllabus) o;

        return id.equals(syllabus.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    //    public LocalDateTime getCreateDateTime() {
//        return createDateTime;
//    }
//
//    public LocalDateTime getUpdateDateTime() {
//        return updateDateTime;
//    }
}
