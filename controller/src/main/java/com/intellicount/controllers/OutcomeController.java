package com.intellicount.controllers;

import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Outcome.Outcome;
import com.intellicount.model.Outcome.OutcomeDTO;
import com.intellicount.model.Outcome.OutcomeListDTO;
import com.intellicount.services.GoalService;
import com.intellicount.services.OutcomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class OutcomeController {
    private final OutcomeService outcomeService;
    private final GoalService goalService;

    @Autowired
    public OutcomeController(OutcomeService outcomeService, GoalService goalService) {
        this.outcomeService = outcomeService;
        this.goalService = goalService;
    }

    @RequestMapping(value="/outcome/", method= RequestMethod.GET)
    public ResponseEntity<List<Outcome>> getOutcomes() {
        return new ResponseEntity<List<Outcome>>(outcomeService.getAll(), HttpStatus.OK);
    }

    @RequestMapping(value="/dev/outcome/", method= RequestMethod.POST)
    public ResponseEntity<List<Outcome>> createOutcome(@RequestBody OutcomeListDTO outcomeListDTO) {
        List<Outcome> returnOutcomes = new ArrayList<>();
        for (OutcomeDTO i : outcomeListDTO.getOutcomes()) {
            Goal goal = goalService.findByName(i.getGoal().getName());
            if (goal == null) {
                return new ResponseEntity("Goal not found", HttpStatus.NOT_FOUND);
            }
            if (outcomeService.findByName(i.getName()) != null ) {
                return new ResponseEntity("Outcome already created", HttpStatus.BAD_REQUEST);
            }
            Outcome newOutcome = i.toTrueClass(goal);
            outcomeService.create(newOutcome);
            returnOutcomes.add(newOutcome);
        }
        return new ResponseEntity<List<Outcome>>(returnOutcomes, HttpStatus.CREATED);
    }
}
