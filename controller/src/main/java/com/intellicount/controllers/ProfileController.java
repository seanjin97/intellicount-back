package com.intellicount.controllers;


import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Goal.GoalDTO;
import com.intellicount.model.Profile.Profile;
import com.intellicount.model.Profile.ProfileDTO;
import com.intellicount.model.Profile.ProfileListDTO;
import com.intellicount.model.ProfileGoals.ProfileGoal;
import com.intellicount.services.GoalService;
import com.intellicount.services.ProfileGoalService;
import com.intellicount.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class ProfileController {
    private final ProfileService profileService;
    private final GoalService goalService;
    private ProfileGoalService profileGoalService;

    @Autowired
    public ProfileController(ProfileService profileService, GoalService goalService, ProfileGoalService profileGoalService) {
        this.profileService = profileService;
        this.goalService = goalService;
        this.profileGoalService = profileGoalService;
    }

    @RequestMapping(value="/profile/", method= RequestMethod.GET)
    public ResponseEntity<Profile> getProfile() {
        List<Profile> profiles = profileService.getAll();
        if (profiles.size() == 0) {
            return new ResponseEntity("No profiles found", HttpStatus.BAD_REQUEST);
        }
        Profile profile = profiles.get(new Random().nextInt(profiles.size()));
        return new ResponseEntity<Profile>(profile, HttpStatus.OK);
    }

    @RequestMapping(value="/dev/profile/", method= RequestMethod.POST)
    public ResponseEntity<List<Profile>> createProfile(@RequestBody ProfileListDTO profileListDTO) {
        List<ProfileDTO> profileDTOs = profileListDTO.getProfiles();
        List<Profile> returnProfiles = new ArrayList<>();
        for (ProfileDTO i : profileDTOs) {
            Profile checkProfile = profileService.findTopByProfile(i.getProfile());
            if (checkProfile != null) {
                return new ResponseEntity("Profile already created", HttpStatus.BAD_REQUEST);
            }
            Profile profile = i.toTrueClass();
            Set<GoalDTO> goalDTOs = i.getGoals();
            Set<ProfileGoal> newGoals = new HashSet<>();
            for (GoalDTO j : goalDTOs) {
                Goal checkGoal = goalService.findByName(j.getName());
                if (checkGoal == null) {
                    checkGoal = j.toTrueClass();
                }
                ProfileGoal profileGoal = new ProfileGoal(profile, checkGoal, j.getValue());
                newGoals.add(profileGoal);
            }
            profile.setProfileGoals(newGoals);
            profileService.create(profile);
            returnProfiles.add(profile);
        }
        return ResponseEntity.ok(returnProfiles);
    }

}
