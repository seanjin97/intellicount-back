package com.intellicount.model.Outcome;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.intellicount.model.Goal.Goal;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class Outcome implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;
    private String name;
    private String financialResult;
    private String success;
    private String failure;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn
    @JsonManagedReference
    private Goal goal;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Outcome() {
    }

    public Outcome(String name, String financialResult, String success, String failure, Goal goal) {
        this.name = name;
        this.financialResult = financialResult;
        this.success = success;
        this.failure = failure;
        this.goal = goal;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFinancialResult() {
        return financialResult;
    }

    public void setFinancialResult(String financialResult) {
        this.financialResult = financialResult;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getFailure() {
        return failure;
    }

    public void setFailure(String failure) {
        this.failure = failure;
    }

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }
}
