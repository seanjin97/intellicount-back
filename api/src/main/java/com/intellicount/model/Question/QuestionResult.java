package com.intellicount.model.Question;

import com.intellicount.model.Syllabus.Syllabus;

import java.io.Serializable;
import java.util.UUID;

public class QuestionResult implements Serializable {
    private UUID id;
    private int numAttempted;
    private int numCorrect;
    private Syllabus syllabus;
    private int questionNum;
    private String text;

    public QuestionResult() {
    }

    public QuestionResult(UUID id) {
        this.id = id;
        this.numAttempted = 0;
        this.numCorrect = 0;
    }
    public QuestionResult(UUID id, int numAttempted, int numCorrect, Syllabus syllabus, int questionNum, String text) {
        this.id = id;
        this.numAttempted = numAttempted;
        this.numCorrect = numCorrect;
        this.syllabus = syllabus;
        this.questionNum = questionNum;
        this.text = text;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getNumAttempted() {
        return numAttempted;
    }

    public void setNumAttempted(int numAttempted) {
        this.numAttempted = numAttempted;
    }

    public int getNumCorrect() {
        return numCorrect;
    }

    public void setNumCorrect(int numCorrect) {
        this.numCorrect = numCorrect;
    }

    public Syllabus getSyllabus() {
        return syllabus;
    }

    public void setSyllabus(Syllabus syllabus) {
        this.syllabus = syllabus;
    }

    public int getQuestionNum() {
        return questionNum;
    }

    public void setQuestionNum(int questionNum) {
        this.questionNum = questionNum;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
