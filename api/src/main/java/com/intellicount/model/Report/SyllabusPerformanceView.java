package com.intellicount.model.Report;

import com.intellicount.model.Question.Question;
import com.intellicount.model.Question.SimpleQuestionId;
import com.intellicount.model.Syllabus.Syllabus;

import java.io.Serializable;
import java.util.List;

public class SyllabusPerformanceView implements Serializable {
    private Syllabus syllabus;
    private double averageNumCorrect;
    private int numQuestions;
    private int numAttempted;
    private int numCorrect;
    private List<SimpleQuestionId> questions;

    public SyllabusPerformanceView() {
    }

    public SyllabusPerformanceView(Syllabus syllabus, double averageNumCorrect, int numQuestions, List<SimpleQuestionId> questions, int numAttempted, int numCorrect) {
        this.syllabus = syllabus;
        this.averageNumCorrect = averageNumCorrect;
        this.numQuestions = numQuestions;
        this.questions = questions;
        this.numAttempted = numAttempted;
        this.numCorrect = numCorrect;
    }

    public Syllabus getSyllabus() {
        return syllabus;
    }

    public void setSyllabus(Syllabus syllabus) {
        this.syllabus = syllabus;
    }

    public double getAverageNumCorrect() {
        return averageNumCorrect;
    }

    public void setAverageNumCorrect(double averageNumCorrect) {
        this.averageNumCorrect = averageNumCorrect;
    }

    public int getNumQuestions() {
        return numQuestions;
    }

    public void setNumQuestions(int numQuestions) {
        this.numQuestions = numQuestions;
    }

    public List<SimpleQuestionId> getQuestions() {
        return questions;
    }

    public void setQuestions(List<SimpleQuestionId> questions) {
        this.questions = questions;
    }

    public int getNumAttempted() {
        return numAttempted;
    }

    public void setNumAttempted(int numAttempted) {
        this.numAttempted = numAttempted;
    }

    public int getNumCorrect() {
        return numCorrect;
    }

    public void setNumCorrect(int numCorrect) {
        this.numCorrect = numCorrect;
    }
}
