package com.intellicount.repositories;

import com.intellicount.model.Session.Session;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface SessionRepository extends CrudRepository<Session, UUID> {
    Session findTopByCode(String code);
}
