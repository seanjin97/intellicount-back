package com.intellicount.services;

import com.intellicount.dao.OutcomeDAO;
import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Outcome.Outcome;
import com.intellicount.repositories.OutcomeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OutcomeService {

    private final OutcomeDAO outcomeDAO;

    @Autowired
    public OutcomeService(@Qualifier("postgresOutcome") OutcomeDAO outcomeDAO) {
        this.outcomeDAO = outcomeDAO;
    }

    public Outcome findById(UUID id) {
        return outcomeDAO.findById(id);
    }

    public List<Outcome> getAll() {
        return outcomeDAO.getAll();
    }

    public Outcome findByName(String name) {
        return outcomeDAO.findByName(name);
    }

    public void create(Outcome outcome) {
        outcomeDAO.create(outcome);
    }
}
