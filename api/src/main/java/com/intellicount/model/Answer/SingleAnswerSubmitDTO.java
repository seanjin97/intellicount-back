package com.intellicount.model.Answer;

import com.intellicount.model.DTO;
import com.intellicount.model.Option.Option;
import com.intellicount.model.Player.Player;
import com.intellicount.model.Question.Question;
import com.intellicount.model.Report.Report;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class SingleAnswerSubmitDTO implements Serializable {
    private UUID questionId;
    private UUID optionId;

    public SingleAnswerSubmitDTO() {
    }

    public UUID getQuestionId() {
        return questionId;
    }

    public void setQuestionId(UUID questionId) {
        this.questionId = questionId;
    }

    public UUID getOptionId() {
        return optionId;
    }

    public void setOptionId(UUID optionId) {
        this.optionId = optionId;
    }

    public Answer toTrueClass(Question question, Option option, Player player, Report report) {
        return new Answer(question, option, player, report );
    }
}
