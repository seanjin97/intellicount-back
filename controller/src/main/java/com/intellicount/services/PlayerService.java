package com.intellicount.services;

import com.intellicount.dao.PlayerDAO;
import com.intellicount.model.Player.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PlayerService {
    private final PlayerDAO playerDAO;
    @Autowired
    public PlayerService(@Qualifier("postgresPlayer") PlayerDAO playerDAO) {
        this.playerDAO = playerDAO;
    }

    public Player create(Player newPlayer) {
        return playerDAO.create(newPlayer);
    }

    public Player findById(UUID id) {
        return playerDAO.findById(id);
    }
}
