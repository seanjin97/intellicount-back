import requests
import json
import random
import string

url = "http://localhost:8080/"

try:
    session = input("enter session code: ").strip()
    numIterations = int(input("how many reports do you want to generate: ").strip())
    
    # retrieve questions
    questions = requests.get(url + 'session/{code}'.format(code=session)).json()['questions']
    
    for iteration in range(numIterations):

        # create player
        player = requests.post(url + "join/", json={"code" : session, "name": ''.join(random.choices(string.ascii_lowercase + string.digits, k=6))}).json()

        # form answer object
        player.pop('name')
        player['player'] = player['id']
        player.pop('id')

        # randomly decide number of questions to answer
        numQuestionsAnswered = random.randint(1, len(questions))

        # generate answers
        l = []
        totalScore = 0
        for i in range(numQuestionsAnswered): 
            answer = {}
            answer['questionId']= questions[i]['id']
            choice = random.choice(questions[i]['options'])
            if choice['correct']:
                totalScore+=1
            answer['optionId'] = choice['id']
            l.append(answer)

        player['answers'] = l
        player['totalScore'] = totalScore

        # submit answer
        response = requests.post(url + 'answer/', json=player)

except Exception as e:
    print('ah fk')