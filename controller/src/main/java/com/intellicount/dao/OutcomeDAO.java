package com.intellicount.dao;

import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Outcome.Outcome;

import java.util.List;
import java.util.UUID;

public interface OutcomeDAO {

    Outcome findById(UUID id);

    List<Outcome> getAll();

    void create (Outcome outcome);

    Outcome findByName(String name);
}
