package com.intellicount.dao;

import com.intellicount.model.Question.Question;

import java.util.UUID;

public interface QuestionDAO {
    Question findById(UUID id);
}
