package com.intellicount.controllers;

import com.intellicount.model.Admin.Admin;
import com.intellicount.model.Answer.Answer;
import com.intellicount.model.Option.Option;
import com.intellicount.model.Option.OptionResult;
import com.intellicount.model.Player.PlayerResult;
import com.intellicount.model.Question.Question;
import com.intellicount.model.Question.QuestionDetailView;
import com.intellicount.model.Question.QuestionResult;
import com.intellicount.model.Question.SimpleQuestionId;
import com.intellicount.model.QuestionList.QuestionList;
import com.intellicount.model.Report.Report;
import com.intellicount.model.Report.SessionOverallView;
import com.intellicount.model.Report.SyllabusPerformanceView;
import com.intellicount.model.Session.Session;
import com.intellicount.model.Syllabus.Syllabus;
import com.intellicount.services.*;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class ReportController {
    private final ReportService reportService;
    private final SessionService sessionService;
    private final QuestionService questionService;
    private final SyllabusService syllabusService;
    private final AdminService adminService;

    @Autowired
    public ReportController(ReportService reportService, SessionService sessionService, QuestionService questionService, SyllabusService syllabusService, AdminService adminService) {
        this.reportService = reportService;
        this.sessionService = sessionService;
        this.questionService = questionService;
        this.syllabusService = syllabusService;
        this.adminService = adminService;
    }

    @GetMapping(value = "/report/{sessionId}")
    public ResponseEntity<List<Report>> retrieveReportsBySession(@PathVariable UUID sessionId) {
        Session session = sessionService.findById(sessionId);
        if (session == null) {
            return new ResponseEntity("Session not found", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(reportService.retrieveBySession(session));
    }

    @GetMapping(value = "/report/session/overview/{sessionId}")
    public ResponseEntity<SessionOverallView> retrieveOverallReportBySession(@PathVariable UUID sessionId) {
        Session session = sessionService.findById(sessionId);
        if (session == null) {
            return new ResponseEntity("Session not found", HttpStatus.NOT_FOUND);
        }
        Set<Report> reports = session.getReports();
        if (reports == null) {
            return new ResponseEntity("No Reports created yet", HttpStatus.NOT_FOUND);
        }
        DescriptiveStatistics stats = new DescriptiveStatistics();
        Set<PlayerResult> playerResults = new HashSet<>();
        Map<Integer, Integer> frequencyMap = new HashMap<>();

        for (Report i : reports) {
            String playerName = i.getPlayer().getName();
            int score = i.getPlayer().getTotalScore();
            PlayerResult playerResult = new PlayerResult(playerName, score);
            playerResults.add(playerResult);
            stats.addValue(score);

            if (frequencyMap.containsKey(score)) {
                frequencyMap.put(score, frequencyMap.get(score) + 1);
            } else {
                frequencyMap.put(score, 1);
            }
        }
        double mean = stats.getMean();
        double standardDeviation = stats.getStandardDeviation();

        SessionOverallView sessionReportView = new SessionOverallView(frequencyMap, playerResults, mean, standardDeviation);
        return ResponseEntity.ok(sessionReportView);
    }

    @GetMapping(value="/report/question/overview/{sessionId}")
    public ResponseEntity<List<QuestionResult>> retrieveOverallQuestionReportBySession(@PathVariable UUID sessionId) {
        Session session = sessionService.findById(sessionId);
        if (session == null) {
            return new ResponseEntity("Session not found", HttpStatus.NOT_FOUND);
        }
        Set<Report> reports = session.getReports();
        if (reports == null) {
            return new ResponseEntity("No Reports created yet", HttpStatus.NOT_FOUND);
        }
        int questionNum = 1;

        List<Question> questions = session.getQuestionList().getQuestions();
        questions.sort(new Comparator<Question>() {
            @Override
            public int compare(Question o1, Question o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        Map<UUID, QuestionResult> questionMap = new LinkedHashMap<>();
        for (Question question : questions) {
            questionMap.put(question.getId(), new QuestionResult(question.getId()));
        }
        for (Report i : reports) {
            List<Answer> answers = i.getAnswers();
            for (Answer j : answers) {
                Question question = j.getQuestion();
                if (questionMap.containsKey(question.getId())) {
                    QuestionResult questionResult = questionMap.get(question.getId());
                    if (questionResult.getNumAttempted() == 0) {
                        Syllabus syllabus = question.getSyllabus();
                        questionResult = new QuestionResult(question.getId(), 1, 0, syllabus, questionNum, question.getText());
                        questionResult.setNumAttempted(1);
                        questionResult.setSyllabus(syllabus);
                        questionResult.setQuestionNum(questionNum);
                        questionResult.setText(question.getText());
                        if (j.getOption().isCorrect()) {
                            questionResult.setNumCorrect(questionResult.getNumCorrect() + 1);
                        }
                        questionMap.put(question.getId(), questionResult);
                        questionNum++;
                    } else {
                        questionResult.setNumAttempted(questionResult.getNumAttempted() + 1);
                        if (j.getOption().isCorrect()) {
                            questionResult.setNumCorrect(questionResult.getNumCorrect() + 1);
                        }
                        questionMap.put(question.getId(), questionResult);
                    }
                }
            }
        }
        questionMap.keySet().removeIf(id -> questionMap.get(id).getNumAttempted() == 0);
        return ResponseEntity.ok(new ArrayList<>(questionMap.values()));
    }

    @GetMapping(value="/report/question/detail/")
    public ResponseEntity<QuestionDetailView> retrievePerformanceByQuestion(@RequestParam UUID sessionId, @RequestParam UUID questionId) {
        Session session = sessionService.findById(sessionId);
        if (session == null) {
            return new ResponseEntity("Session not found", HttpStatus.NOT_FOUND);
        }
        Set<Report> reports = session.getReports();
        if (reports == null) {
            return new ResponseEntity("No Reports created yet", HttpStatus.NOT_FOUND);
        }
        Question question = questionService.findById(questionId);
        if (question == null) {
            return new ResponseEntity("Question does not exist", HttpStatus.NOT_FOUND);
        }
        Map<String, OptionResult> optionMap = new HashMap<>();
        for (Option i : question.getOptions()) {
            optionMap.put(i.getText(), new OptionResult( 0, i.isCorrect(),0 ));
        }
        int numAttempted = 0;
        for (Report i : reports) {
            List<Answer> answers = i.getAnswers();
            for (Answer j : answers) {
                if (j.getQuestion().equals(question)) {
                    numAttempted++;
                    if (optionMap.containsKey(j.getOption().getText())) {
                        OptionResult optionResult = optionMap.get(j.getOption().getText());
                        int numChosen = optionResult.getNumChosen() + 1;
                        optionResult.setNumChosen(numChosen);
                        if (j.getOption().isCorrect()) {
                            int numCorrect = optionResult.getNumCorrect() + 1;
                            optionResult.setNumCorrect(numCorrect);
                        }
                        optionMap.put(j.getOption().getText(), optionResult);
                    }
                }
            }
        }
        QuestionDetailView questionDetailView = new QuestionDetailView(question.getText(), question.getDifficulty(), optionMap, question.getSyllabus(), numAttempted);
        return ResponseEntity.ok(questionDetailView);
    }

    @GetMapping(value="/report/question/syllabus/overview/{sessionId}")
    public ResponseEntity<List<SyllabusPerformanceView>> retrievePerformanceBySyllabus(@PathVariable UUID sessionId) {
        Session session = sessionService.findById(sessionId);
        if (session == null) {
            return new ResponseEntity("Session not found", HttpStatus.NOT_FOUND);
        }
        Set<Report> reports = session.getReports();
        List<Syllabus> syllabi = syllabusService.getAll();

        Map<Syllabus, Map<String, Integer>> frequencyMap = new HashMap<>();
        for (Syllabus i : syllabi) {
            HashMap<String, Integer> tempMap = new HashMap<>();
            tempMap.put("sum", 0);
            tempMap.put("freq", 0);
            frequencyMap.put(i, tempMap);
        }
        Map<Syllabus, SyllabusPerformanceView> syllabusPerformanceViewMap = new HashMap<>();

        for (Report i : reports) {
            List<Answer> answers = i.getAnswers();
            int idx = 1;
            for (Answer j : answers) {
                Question question = j.getQuestion();
                SimpleQuestionId simpleQuestionId = new SimpleQuestionId(idx, question.getId());
                Syllabus syllabus = question.getSyllabus();
                Option option = j.getOption();
                Map<String, Integer> map = frequencyMap.get(syllabus);
                map.put("freq", map.get("freq") + 1);
                if (option.isCorrect()) {
                    map.put("sum", map.get("sum") + 1);
                }
                double avg = ((double) map.get("sum")) / map.get("freq");
                if (syllabusPerformanceViewMap.containsKey(syllabus)) {
                    SyllabusPerformanceView syllabusPerformanceView = syllabusPerformanceViewMap.get(syllabus);
                    List<SimpleQuestionId> questions = syllabusPerformanceView.getQuestions();
                    if (!(questions.contains(simpleQuestionId))){
                        syllabusPerformanceView.setNumQuestions(syllabusPerformanceView.getNumQuestions() + 1);
                        questions.add(simpleQuestionId);
                        syllabusPerformanceView.setQuestions(questions);
                    }
                    syllabusPerformanceView.setAverageNumCorrect(avg);
                    syllabusPerformanceView.setNumAttempted(map.get("freq"));
                    syllabusPerformanceView.setNumCorrect(map.get("sum"));
                } else {
                    SyllabusPerformanceView syllabusPerformanceView = new SyllabusPerformanceView(syllabus, avg, 1, new ArrayList<>(Collections.singletonList(simpleQuestionId)), map.get("freq"), map.get("sum"));
                    syllabusPerformanceViewMap.put(syllabus, syllabusPerformanceView);
                }
                idx++;
            }
        }
        List<SyllabusPerformanceView> syllabusPerformanceViews = new ArrayList<>(syllabusPerformanceViewMap.values());
        for (SyllabusPerformanceView i : syllabusPerformanceViews) {
            i.setAverageNumCorrect(Math.round(i.getAverageNumCorrect() * 100) / 100.0);
        }
        return ResponseEntity.ok(syllabusPerformanceViews);
    }

    @GetMapping(value="/report/verify-if-exists/{adminId}")
    public ResponseEntity verifyIfSessionsHaveReports(@PathVariable UUID adminId) {
        Admin admin = adminService.findById(adminId);
        Set<Session> sessions = admin.getSessions();
        List<UUID> verificationList = new ArrayList<>();
        for (Session i : sessions) {
            Set<Report> reports = i.getReports();
            if (reports != null && reports.size() > 0) {
                verificationList.add(i.getId());
            }
        }
        return ResponseEntity.ok(verificationList);
    }
}
