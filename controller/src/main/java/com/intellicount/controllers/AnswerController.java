package com.intellicount.controllers;

import com.intellicount.model.Answer.Answer;
import com.intellicount.model.Answer.AnswerSubmitDTO;
import com.intellicount.model.Answer.SingleAnswerSubmitDTO;
import com.intellicount.model.Option.Option;
import com.intellicount.model.Player.Player;
import com.intellicount.model.Question.Question;
import com.intellicount.model.Report.Report;
import com.intellicount.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class AnswerController {
    private final AnswerService answerService;
    private final PlayerService playerService;
    private final QuestionService questionService;
    private final OptionService optionService;
    private final ReportService reportService;

    @Autowired
    public AnswerController(AnswerService answerService, PlayerService playerService, QuestionService questionService, OptionService optionService, ReportService reportService) {
        this.answerService = answerService;
        this.playerService = playerService;
        this.questionService = questionService;
        this.optionService = optionService;
        this.reportService = reportService;
    }

    @PostMapping(value = "/answer/")
    public ResponseEntity<String> submitAnswers(@RequestBody AnswerSubmitDTO answerSubmitDTO) {
        UUID playerId = answerSubmitDTO.getPlayer();
        Player player = playerService.findById(playerId);
        if (player == null) {
            return ResponseEntity.notFound().build();
        }
        int streak = answerSubmitDTO.getTotalScore();
        List<SingleAnswerSubmitDTO> singleAnswerSubmitDTOs = answerSubmitDTO.getAnswers();
        player.setTotalScore(streak);
        if (reportService.findByPlayer(player) != null) {
            return new ResponseEntity("This player has already submitted their answers", HttpStatus.BAD_REQUEST);
        }
        Report newReport = new Report(player.getSession(), player);

        for (SingleAnswerSubmitDTO i : singleAnswerSubmitDTOs) {
            Question question = questionService.findById(i.getQuestionId());
            if (question == null) {
                return ResponseEntity.notFound().build();
            }
            Option option = optionService.findById(i.getOptionId());
            if (option == null || !(option.getQuestion().equals(question))) {
                return ResponseEntity.notFound().build();
            }
            Answer newAnswer = i.toTrueClass(question, option, player, newReport);
            answerService.create(newAnswer);
        }
        playerService.create(player);
        return ResponseEntity.ok("SUBMITTED");
    }
}
