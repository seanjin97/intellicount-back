package com.intellicount.dao;

import com.intellicount.model.Player.Player;
import com.intellicount.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository("postgresPlayer")
public class PostgresPlayerDataAccessService implements PlayerDAO{
    private final PlayerRepository playerRepository;

    @Autowired
    public PostgresPlayerDataAccessService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Override
    public Player create(Player player) {
        return playerRepository.save(player);
    }

    @Override
    public Player findById(UUID id) {
        return playerRepository.findById(id).orElse(null);
    }


}
