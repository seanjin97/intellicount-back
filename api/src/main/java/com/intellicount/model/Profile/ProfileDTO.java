package com.intellicount.model.Profile;

import com.intellicount.model.DTO;
import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Goal.GoalDTO;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Set;

public class ProfileDTO implements Serializable, DTO {

    @NotBlank
    private String profile;

    @NotBlank
    private String company;

    @NotBlank
    private String message;

    @NotBlank
    private double initialRevenue;

    @NotBlank
    private double initialCost;

    private Set<GoalDTO> goals;

    public ProfileDTO() {
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getInitialRevenue() {
        return initialRevenue;
    }

    public void setInitialRevenue(double initialRevenue) {
        this.initialRevenue = initialRevenue;
    }

    public double getInitialCost() {
        return initialCost;
    }

    public void setInitialCost(double initialCost) {
        this.initialCost = initialCost;
    }

    public Set<GoalDTO> getGoals() {
        return goals;
    }

    public void setGoals(Set<GoalDTO> goals) {
        this.goals = goals;
    }

    @Override
    public Profile toTrueClass() {
        return new Profile(
                this.profile, this.company, this.message, this.initialRevenue, this.initialCost
        );
    }

}

