package com.intellicount.model.Session;

import java.io.Serializable;

public class SessionJoinDTO implements Serializable {
    private String name;
    private String code;

    public SessionJoinDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
