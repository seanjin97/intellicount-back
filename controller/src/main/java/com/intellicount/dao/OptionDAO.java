package com.intellicount.dao;

import com.intellicount.model.Option.Option;

import java.util.UUID;

public interface OptionDAO {
    Option findById(UUID id);
    Option create(Option option);
}
