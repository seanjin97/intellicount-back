package com.intellicount.repositories;

import com.intellicount.model.Decision.Decision;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface DecisionRepository extends CrudRepository<Decision, UUID>{
}


