package com.intellicount.services;

import com.intellicount.dao.ScenarioDAO;
import com.intellicount.model.Decision.Decision;
import com.intellicount.model.Scenario.Scenario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ScenarioService {

    private final ScenarioDAO scenarioDAO;

    @Autowired
    public ScenarioService(@Qualifier("postgresScenario") ScenarioDAO scenarioDAO) {
        this.scenarioDAO = scenarioDAO;
    }

    public Scenario findById(UUID id) {
        return scenarioDAO.findById(id);
    }

    public List<Scenario> getAll() {
        return scenarioDAO.getAll();
    }

    public void create(Scenario scenario) {
        scenarioDAO.create(scenario);
    }

    public void update (Scenario updatedScenario){
        Scenario scenarioToBeUpdated = scenarioDAO.findById(updatedScenario.getId());
        scenarioToBeUpdated.setQuestion(updatedScenario.getQuestion());
        scenarioToBeUpdated.setText(updatedScenario.getText());
        scenarioDAO.update(scenarioToBeUpdated);
    }

    public void delete (Scenario scenario) {
        scenarioDAO.delete(scenario);
    }

    public void decisionToScenario (Decision decision){
        scenarioDAO.createDecision(decision);
    }

    public Scenario findTopByQuestion(String question) {
        return scenarioDAO.findTopByQuestion(question);
    }

}