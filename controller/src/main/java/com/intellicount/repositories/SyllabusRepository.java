package com.intellicount.repositories;

import com.intellicount.model.Syllabus.Syllabus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface SyllabusRepository extends JpaRepository<Syllabus, UUID> {
    Syllabus findTopByChapterAndName(String chapter, String name);
}
