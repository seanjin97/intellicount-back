package com.intellicount.model.Goal;

import com.intellicount.model.Profile.Profile;

import java.io.Serializable;

public class GoalDTO implements Serializable {
    private String name;
    private double value;

    public GoalDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Goal toTrueClass() {
        return new Goal(this.name);
    }
}
