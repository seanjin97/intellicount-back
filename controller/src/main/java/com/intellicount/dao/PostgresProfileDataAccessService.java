package com.intellicount.dao;

import com.intellicount.model.Goal.Goal;
import com.intellicount.model.Profile.Profile;
import com.intellicount.repositories.DecisionRepository;
import com.intellicount.repositories.GoalRepository;
import com.intellicount.repositories.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;


@Repository("postgresProfile")
public class PostgresProfileDataAccessService implements ProfileDAO{

    private final ProfileRepository profileRepository;
    private final GoalRepository goalRepository;

    @Autowired
    public PostgresProfileDataAccessService(
            ProfileRepository profileRepository, GoalRepository goalRepository) {
        this.profileRepository = profileRepository;
        this.goalRepository = goalRepository;

    }

    @Override
    public Profile findById(UUID id) {
        return profileRepository.findById(id).orElse(null);
    }

    @Override
    public List<Profile> getAll() {
        return profileRepository.findAll();
    }

    @Override
    public void create(Profile profile) {
        profileRepository.save(profile);
    }

    @Override
    public void createGoal(Goal goal) {
        goalRepository.save(goal);
    }

    @Override
    public Profile findTopByProfile(String profile) {
        return profileRepository.findTopByProfile(profile);
    }
}
