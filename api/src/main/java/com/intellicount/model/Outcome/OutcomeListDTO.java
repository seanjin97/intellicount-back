package com.intellicount.model.Outcome;

import java.io.Serializable;
import java.util.List;

public class OutcomeListDTO implements Serializable {
    private List<OutcomeDTO> outcomes;

    public OutcomeListDTO() {
    }

    public List<OutcomeDTO> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(List<OutcomeDTO> outcomes) {
        this.outcomes = outcomes;
    }
}
