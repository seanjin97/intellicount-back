package com.intellicount.model.Decision;

import com.fasterxml.jackson.annotation.*;
import com.intellicount.model.Scenario.Scenario;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class Decision implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;
    private String text;
    private double cash;
    private double efficiency;
    private double reputation;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn
    @JsonBackReference
    private Scenario scenario;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Decision() {
    }

    public Decision(String text, double cash, double efficiency, double reputation, Scenario scenario) {
        this.text = text;
        this.cash = cash;
        this.efficiency = efficiency;
        this.reputation = reputation;
        this.scenario = scenario;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(double efficiency) {
        this.efficiency = efficiency;
    }

    public double getReputation() {
        return reputation;
    }

    public void setReputation(double reputation) {
        this.reputation = reputation;
    }

    public Scenario getScenario() {
        return scenario;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }
}
