package com.intellicount.dao;

import com.intellicount.model.Admin.Admin;

import java.util.Optional;
import java.util.UUID;

public interface AdminDAO {
    Admin findByID(UUID id);

    Admin create(Admin admin);

    Admin findByName(String name);
}
