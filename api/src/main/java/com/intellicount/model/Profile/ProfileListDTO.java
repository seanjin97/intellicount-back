package com.intellicount.model.Profile;

import java.util.List;

public class ProfileListDTO {
    private List<ProfileDTO> profiles;

    public ProfileListDTO() {
    }

    public List<ProfileDTO> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<ProfileDTO> profiles) {
        this.profiles = profiles;
    }
}
