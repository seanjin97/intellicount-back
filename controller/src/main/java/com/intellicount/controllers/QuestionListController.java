package com.intellicount.controllers;

import com.intellicount.model.Admin.Admin;
import com.intellicount.model.Option.Option;
import com.intellicount.model.Option.OptionDTO;
import com.intellicount.model.Question.Question;
import com.intellicount.model.Question.QuestionDTO;
import com.intellicount.model.QuestionList.*;
import com.intellicount.model.Syllabus.Syllabus;
import com.intellicount.services.AdminService;
import com.intellicount.services.OptionService;
import com.intellicount.services.QuestionListService;
import com.intellicount.services.SyllabusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class QuestionListController {

    private final AdminService adminService;
    private final QuestionListService questionListService;
    private final OptionService optionService;
    private final SyllabusService syllabusService;

    @Autowired
    public QuestionListController(AdminService adminService, QuestionListService questionListService, OptionService optionService, SyllabusService syllabusService) {
        this.adminService = adminService;
        this.questionListService = questionListService;
        this.optionService = optionService;
        this.syllabusService = syllabusService;
    }

    @PostMapping(value = "/quiz/base/")
    public ResponseEntity<QuestionList> createQuiz(@RequestBody QuestionListEmptyDTO questionListEmptyDTO) {
        Admin admin = adminService.findById(questionListEmptyDTO.getAdmin());
            if (admin == null) {
            return ResponseEntity.notFound().build();
        }
        QuestionList newQuestionList = questionListEmptyDTO.toTrueClass();
        newQuestionList.setType("CUSTOM");
        newQuestionList.addAdmin(admin);
        questionListService.create(newQuestionList);
        return ResponseEntity.ok(newQuestionList);
    }

    @GetMapping(value="/quiz/admin/{adminId}")
    public ResponseEntity<Set<QuestionList>> retrieveQuestionLists(@PathVariable UUID adminId) {
        Admin admin = adminService.findById(adminId);
        if (admin == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(admin.getQuestionLists());
    }

    @GetMapping(value="/quiz/{questionListId}")
    public ResponseEntity<List<Question>> retrieveQuestions(@PathVariable UUID questionListId) {
        QuestionList questionList = questionListService.findById(questionListId);
        if (questionList == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(questionList.getQuestions());
    }

    @PostMapping(value = "/quiz/questions/")
    public ResponseEntity<QuestionList> createNewQuestions(@RequestBody QuestionListCreateDTO questionListCreateDTO) {
        Admin admin = adminService.findById(questionListCreateDTO.getAdminId());
        if (admin == null) {
            return ResponseEntity.notFound().build();
        }
        QuestionList newQuestionList = new QuestionList(questionListCreateDTO.getName());
        newQuestionList.addAdmin(admin);
        newQuestionList.setType("CUSTOM");
        List<Question> questions = new ArrayList<>();
        for (QuestionDTO i : questionListCreateDTO.getQuestions()) {
            Question newQuestion = i.toTrueClass(newQuestionList);
            String chapter = i.getSyllabus().getChapter();
            String syllabusName = i.getSyllabus().getName();
            Syllabus checkSyllabus = syllabusService.findTopByChapterAndName(chapter, syllabusName);
            if (checkSyllabus == null) {
                Syllabus newSyllabus = i.getSyllabus().toTrueClass();
                newQuestion.setSyllabus(newSyllabus);
            } else {
                newQuestion.setSyllabus(checkSyllabus);
            }
            // Check and covert difficulty into Points
            if (newQuestion.getDifficulty().equals("Easy")) {
                newQuestion.setReputation(1);
                newQuestion.setEfficiency(1);
            } else if (newQuestion.getDifficulty().equals("Medium")) {
                newQuestion.setReputation(2);
                newQuestion.setEfficiency(2);
            } else if (newQuestion.getDifficulty().equals("Hard")) {
                newQuestion.setReputation(3);
                newQuestion.setEfficiency(3);
            } else {
                return ResponseEntity.badRequest().build();
            }
            List<Option> options = new ArrayList<>();
            for (OptionDTO j : i.getOptions()) {
                Option newOption = j.toTrueClass(newQuestion);
                if (newOption != null) {
                    options.add(newOption);
                    optionService.create(newOption);
                }
            }
            newQuestion.setOptions(options);
            questions.add(newQuestion);
        }
        newQuestionList.setQuestions(questions);
        return ResponseEntity.ok(newQuestionList);
    }


    /* create question and options on API call, don't modify existing rows because we want to maintain
    answers attached to the previous Question and Option, Report will not track the Question List */
    @PutMapping(value = "/quiz/edit/")
    public ResponseEntity<QuestionList> modifyQuestions(@RequestBody QuestionListModifyDTO questionListModifyDTO) {
        Admin admin = adminService.findById(questionListModifyDTO.getAdmin());
        if (admin == null)  {
            return new ResponseEntity("Invalid credentials", HttpStatus.NOT_FOUND);
        }

        // create new question list
        QuestionList newQuestionList = new QuestionList(questionListModifyDTO.getName());
        newQuestionList.addAdmin(admin);
        newQuestionList.setType("CUSTOM");
        List<Question> questions = new ArrayList<>();
        for (QuestionDTO i : questionListModifyDTO.getQuestions()) {
            Question newQuestion = i.toTrueClass(newQuestionList);
            String chapter = i.getSyllabus().getChapter();
            String syllabusName = i.getSyllabus().getName();
            Syllabus checkSyllabus = syllabusService.findTopByChapterAndName(chapter, syllabusName);
            if (checkSyllabus == null) {
                return new ResponseEntity("Syllabus not found", HttpStatus.NOT_FOUND);
            } else {
                newQuestion.setSyllabus(checkSyllabus);
            }
            // Check and covert difficulty into Points
            if (newQuestion.getDifficulty().equals("Easy")) {
                newQuestion.setReputation(1);
                newQuestion.setEfficiency(1);
            } else if (newQuestion.getDifficulty().equals("Medium")) {
                newQuestion.setReputation(2);
                newQuestion.setEfficiency(2);
            } else if (newQuestion.getDifficulty().equals("Hard")) {
                newQuestion.setReputation(3);
                newQuestion.setEfficiency(3);
            } else {
                return ResponseEntity.badRequest().build();
            }

            List<Option> options = new ArrayList<>();
            for (OptionDTO j : i.getOptions()) {
                Option newOption = j.toTrueClass(newQuestion);
                if (newOption != null) {
                    options.add(newOption);
                    optionService.create(newOption);
                }
            }
            newQuestion.setOptions(options);
            questions.add(newQuestion);
        }
        newQuestionList.setQuestions(questions);

        QuestionList existingQuestionList = questionListService.findById(questionListModifyDTO.getQuestionList());
        // dissociate existing question list from admin and session if the Type is 'CUSTOM'
        if (existingQuestionList != null && existingQuestionList.getType().equals("CUSTOM")) {
            existingQuestionList.removeAdmin(admin);
            existingQuestionList.setSessions(null);
            questionListService.create(existingQuestionList);
        }
        return ResponseEntity.ok(newQuestionList);
    }

    @PostMapping(value = "/dev/quiz/")
    public ResponseEntity<QuestionList> defaultQuestionList(@RequestBody QuestionListDevDTO questionListDevDTO) {
        QuestionList newQuestionList = new QuestionList(questionListDevDTO.getName());
        newQuestionList.setType("DEFAULT");
        List<Question> questions = new ArrayList<>();
        for (QuestionDTO i : questionListDevDTO.getQuestions()) {
            Question newQuestion = i.toTrueClass(newQuestionList);
            String chapter = i.getSyllabus().getChapter();
            String syllabusName = i.getSyllabus().getName();
            Syllabus checkSyllabus = syllabusService.findTopByChapterAndName(chapter, syllabusName);
            if (checkSyllabus == null) {
                Syllabus newSyllabus = i.getSyllabus().toTrueClass();
                newQuestion.setSyllabus(newSyllabus);
            } else {
                newQuestion.setSyllabus(checkSyllabus);
            }
            // Check and covert difficulty into Points
            if (newQuestion.getDifficulty().equals("Easy")) {
                newQuestion.setReputation(1);
                newQuestion.setEfficiency(1);
            } else if (newQuestion.getDifficulty().equals("Medium")) {
                newQuestion.setReputation(2);
                newQuestion.setEfficiency(2);
            } else if (newQuestion.getDifficulty().equals("Hard")) {
                newQuestion.setReputation(3);
                newQuestion.setEfficiency(3);
            } else {
                return ResponseEntity.badRequest().build();
            }
            List<Option> options = new ArrayList<>();
            for (OptionDTO j : i.getOptions()) {
                Option newOption = j.toTrueClass(newQuestion);
                options.add(newOption);
                optionService.create(newOption);
            }
            newQuestion.setOptions(options);
            questions.add(newQuestion);
        }
        newQuestionList.setQuestions(questions);
        return ResponseEntity.ok(newQuestionList);
    }

    @DeleteMapping(value="/quiz/edit/")
    public ResponseEntity<QuestionList> deleteQuestionList(@RequestBody QuestionListDeleteDTO questionListDeleteDTO) {
        Admin admin = adminService.findById(questionListDeleteDTO.getAdmin());
        if (admin == null) {
            return new ResponseEntity("Admin not valid", HttpStatus.NOT_FOUND);
        }
        QuestionList questionList = questionListService.findById(questionListDeleteDTO.getQuestionList());
        if (questionList == null) {
            return new ResponseEntity("Question List not found", HttpStatus.NOT_FOUND);
        }
        if (questionList.getType().equals("DEFAULT")) {
            return new ResponseEntity("This Question List is not allowed to be deleted", HttpStatus.BAD_REQUEST);
        }
        questionList.removeAdmin(admin);
        questionList.setSessions(null);
        // don't remove questions from question list so that reports
        // will still continue to be able to retrieve the questions data
//        if (questionList.getAdmins().size() == 0) {
//            questionListService.delete(questionList);
//        }
        questionListService.create(questionList);
        return ResponseEntity.ok(questionList);
    }
}
