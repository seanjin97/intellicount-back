package com.intellicount.model.Player;

import java.io.Serializable;

public class PlayerResult implements Serializable {
    private String name;
    private int score;

    public PlayerResult() {
    }

    public PlayerResult(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
