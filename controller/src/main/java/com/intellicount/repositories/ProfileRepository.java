package com.intellicount.repositories;

import com.intellicount.model.Profile.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProfileRepository extends JpaRepository<Profile, UUID> {
    Profile findTopByProfile(String profile);
}
