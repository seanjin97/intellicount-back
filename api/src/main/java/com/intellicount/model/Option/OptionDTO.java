package com.intellicount.model.Option;

import com.intellicount.model.DTO;
import com.intellicount.model.Question.Question;

public class OptionDTO {
    private String text;
    private boolean correct;

    public OptionDTO() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public Option toTrueClass(Question question) {
        return new Option(this.text, this.correct, question);
    }
}
