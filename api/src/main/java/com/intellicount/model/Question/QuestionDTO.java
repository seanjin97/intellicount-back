package com.intellicount.model.Question;

import com.intellicount.model.DTO;
import com.intellicount.model.Option.OptionDTO;
import com.intellicount.model.QuestionList.QuestionList;
import com.intellicount.model.Syllabus.SyllabusDTO;

import java.io.Serializable;
import java.util.Set;

public class QuestionDTO implements Serializable {
    private String text;
    private String difficulty;
    private String result;
    private Set<OptionDTO> options;
    private SyllabusDTO syllabus;

    public QuestionDTO() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Set<OptionDTO> getOptions() {
        return options;
    }

    public void setOptions(Set<OptionDTO> options) {
        this.options = options;
    }

    public SyllabusDTO getSyllabus() {
        return syllabus;
    }

    public void setSyllabus(SyllabusDTO syllabus) {
        this.syllabus = syllabus;
    }

    public Question toTrueClass(QuestionList questionList) {
        return new Question(this.text, this.difficulty, this.result, questionList);
    }
}
