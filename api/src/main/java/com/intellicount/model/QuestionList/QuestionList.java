package com.intellicount.model.QuestionList;

import com.fasterxml.jackson.annotation.*;
import com.intellicount.model.Admin.Admin;
import com.intellicount.model.Question.Question;
import com.intellicount.model.Session.Session;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
public class QuestionList implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;
    private String name;
    private String type;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "questionLists")
    @JsonBackReference
    private Set<Admin> admins = new HashSet<>();

    @OneToMany(mappedBy = "questionList", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Question> questions;

    @OneToMany(mappedBy = "questionList", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonBackReference
    private Set<Session> sessions;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public QuestionList() {
    }

    public QuestionList(String name) {
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public Set<Admin> getAdmins() {
        return admins;
    }

    public void setAdmins(Set<Admin> admins) {
        this.admins = admins;
    }

    public Set<Session> getSessions() {
        return sessions;
    }

    public void setSessions(Set<Session> sessions) {
        this.sessions = sessions;
    }

    public void addAdmin(Admin admin) {
        this.admins.add(admin);
        Set<QuestionList> retrieveQuestionLists = admin.getQuestionLists();
        retrieveQuestionLists.add(this);
        admin.setQuestionLists(retrieveQuestionLists);
    }

    public void removeAdmin(Admin admin) {
        this.admins.remove(admin);
        Set<QuestionList> retrieveQuestionLists = admin.getQuestionLists();
        retrieveQuestionLists.remove(this);
        admin.setQuestionLists(retrieveQuestionLists);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "QuestionList{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", admins=" + admins +
                ", questions=" + questions +
                ", sessions=" + sessions +
                '}';
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }
}
