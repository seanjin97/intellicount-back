package com.intellicount.repositories;

import com.intellicount.model.QuestionList.QuestionList;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;
import java.util.UUID;

public interface QuestionListRepository extends CrudRepository<QuestionList, UUID> {
    Set<QuestionList> findAllByType(String type);
}
