package com.intellicount.controllers;

import com.intellicount.model.Decision.Decision;
import com.intellicount.model.Decision.DecisionDTO;
import com.intellicount.model.Scenario.Scenario;
import com.intellicount.model.Scenario.ScenarioDTO;
import com.intellicount.model.Scenario.ScenarioSetDTO;
import com.intellicount.services.ScenarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class ScenarioController {
    private final ScenarioService scenarioService;

    @Autowired
    public ScenarioController(ScenarioService scenarioService) {
        this.scenarioService = scenarioService;
    }

    @RequestMapping(value = "/scenarios/", method = RequestMethod.GET)
    public ResponseEntity<List<Scenario>> getScenario() {
        return new ResponseEntity<List<Scenario>>(scenarioService.getAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/dev/scenario/", method = RequestMethod.POST)
    public ResponseEntity<Set<Scenario>> createScenarioSet(@RequestBody ScenarioSetDTO scenarioSetDTO) {
        Set<Scenario> newScenarioSet = new HashSet<>();
        for (ScenarioDTO i : scenarioSetDTO.getScenarios()) {
            Scenario checkScenario = scenarioService.findTopByQuestion(i.getQuestion());
            // if Scenario question exists stop creating the new Scenario
            if (checkScenario != null) {
                return ResponseEntity.badRequest().build();
            }
            Scenario newScenario = i.toTrueClass();
            Set<DecisionDTO> decisionDTOs = i.getDecisions();
            Set<Decision> decisions = new HashSet<>();
            for (DecisionDTO decision : decisionDTOs) {
                Decision newDecision = decision.toTrueClass(newScenario);
                scenarioService.decisionToScenario(newDecision);
                decisions.add(newDecision);
            }
            newScenario.setDecisions(decisions);
            newScenarioSet.add(newScenario);
        }
        return new ResponseEntity<Set<Scenario>>(newScenarioSet, HttpStatus.CREATED);
    }
}