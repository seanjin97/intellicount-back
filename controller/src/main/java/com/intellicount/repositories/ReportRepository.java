package com.intellicount.repositories;

import com.intellicount.model.Player.Player;
import com.intellicount.model.Report.Report;
import com.intellicount.model.Session.Session;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface ReportRepository extends CrudRepository<Report, UUID>{
    List<Report> findAllBySession(Session session);
    Report findTopByPlayer(Player player);
}
