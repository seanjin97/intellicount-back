package com.intellicount.repositories;

import com.intellicount.model.Admin.Admin;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface AdminRepository extends CrudRepository<Admin, UUID> {
    Admin findTopByName(String name);
}
