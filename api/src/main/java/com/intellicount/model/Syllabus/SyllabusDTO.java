package com.intellicount.model.Syllabus;

import com.intellicount.model.DTO;

import java.io.Serializable;

public class SyllabusDTO implements Serializable, DTO {
    private String chapter;
    private String name;

    public SyllabusDTO() {
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Syllabus toTrueClass() {
        return new Syllabus(this.chapter, this.name);
    }
}
