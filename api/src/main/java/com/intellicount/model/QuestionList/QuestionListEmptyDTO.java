package com.intellicount.model.QuestionList;

import com.intellicount.model.Admin.Admin;
import com.intellicount.model.DTO;

import java.io.Serializable;
import java.util.UUID;

public class QuestionListEmptyDTO implements Serializable{
    private UUID admin;
    private String questionListName;

    public QuestionListEmptyDTO() {
    }

    public UUID getAdmin() {
        return admin;
    }

    public void setAdmin(UUID admin) {
        this.admin = admin;
    }

    public String getQuestionListName() {
        return questionListName;
    }

    public void setQuestionListName(String questionListName) {
        this.questionListName = questionListName;
    }

    public QuestionList toTrueClass() {
        return new QuestionList(this.questionListName);
    }
}
