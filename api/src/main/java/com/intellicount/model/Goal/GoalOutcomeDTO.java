package com.intellicount.model.Goal;

import java.io.Serializable;

public class GoalOutcomeDTO implements Serializable {
    private String name;

    public GoalOutcomeDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
