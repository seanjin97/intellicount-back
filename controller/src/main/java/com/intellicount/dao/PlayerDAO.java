package com.intellicount.dao;

import com.intellicount.model.Player.Player;

import java.util.UUID;

public interface PlayerDAO {
    Player create(Player player);

    Player findById(UUID id);
}
