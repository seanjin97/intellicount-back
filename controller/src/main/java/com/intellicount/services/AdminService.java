package com.intellicount.services;

import com.intellicount.dao.AdminDAO;
import com.intellicount.model.Admin.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AdminService {
    private final AdminDAO adminDAO;

    @Autowired
    public AdminService(@Qualifier("postgresAdmin") AdminDAO adminDAO) {
        this.adminDAO = adminDAO;
    }

    public Admin findById(UUID id) {
        return adminDAO.findByID(id);
    }

    public Admin create(Admin admin) {
        return adminDAO.create(admin);
    }

    public Admin findByUser(String name) {
        return adminDAO.findByName(name);
    }
}
