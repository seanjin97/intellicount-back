package com.intellicount.services;

import com.intellicount.dao.AnswerDAO;
import com.intellicount.model.Answer.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AnswerService {
    private final AnswerDAO answerDAO;

    @Autowired
    public AnswerService(@Qualifier("postgresAnswer") AnswerDAO answerDAO) {
        this.answerDAO = answerDAO;
    }

    public Answer create(Answer answer) {
        return answerDAO.create(answer);
    }
}
