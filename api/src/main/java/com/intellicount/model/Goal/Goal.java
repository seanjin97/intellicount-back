package com.intellicount.model.Goal;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.intellicount.model.Outcome.Outcome;
import com.intellicount.model.ProfileGoals.ProfileGoal;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Entity
public class Goal implements Serializable {
    @Id
    @GeneratedValue
    private UUID id;
    private String name;

    @OneToMany(mappedBy = "goal")
    @JsonBackReference
    Set<ProfileGoal> profileGoals;

    @OneToOne(mappedBy = "goal", fetch = FetchType.EAGER)
    @JsonBackReference
    private Outcome outcome;

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public Goal() {
    }

    public Goal(String name) {
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Outcome getOutcome() {
        return outcome;
    }

    public void setOutcome(Outcome outcome) {
        this.outcome = outcome;
    }

    public Set<ProfileGoal> getProfileGoals() {
        return profileGoals;
    }

    public void setProfileGoals(Set<ProfileGoal> profileGoals) {
        this.profileGoals = profileGoals;
    }
}
