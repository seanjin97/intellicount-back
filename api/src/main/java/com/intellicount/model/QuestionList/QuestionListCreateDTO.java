package com.intellicount.model.QuestionList;

import com.intellicount.model.Question.QuestionDTO;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public class QuestionListCreateDTO {
    private String name;
    private UUID adminId;
    private List<QuestionDTO> questions;

    public QuestionListCreateDTO() {
    }

    public List<QuestionDTO> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionDTO> questions) {
        this.questions = questions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getAdminId() {
        return adminId;
    }

    public void setAdminId(UUID adminId) {
        this.adminId = adminId;
    }
}
