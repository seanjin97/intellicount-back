package com.intellicount.dao;

import com.intellicount.model.Decision.Decision;
import com.intellicount.model.Scenario.Scenario;

import java.util.List;
import java.util.UUID;

public interface ScenarioDAO {

    Scenario findById(UUID id);

    List<Scenario> getAll();

    void create(Scenario scenario);

    void update (Scenario updatedScenario);

    void delete (Scenario scenario);

    void createDecision(Decision decision);

    Scenario findTopByQuestion(String question);
}
