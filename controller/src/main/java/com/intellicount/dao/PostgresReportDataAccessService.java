package com.intellicount.dao;

import com.intellicount.model.Player.Player;
import com.intellicount.model.Report.Report;
import com.intellicount.model.Session.Session;
import com.intellicount.repositories.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository("postgresReport")
public class PostgresReportDataAccessService implements  ReportDAO{
    private final ReportRepository reportRepository;

    @Autowired
    public PostgresReportDataAccessService(ReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }

    @Override
    public Report create(Report report) {
        return reportRepository.save(report);
    }

    @Override
    public List<Report> retrieveBySession(Session session) {
        return reportRepository.findAllBySession(session);
    }

    @Override
    public Report findByPlayer(Player player) {
        return reportRepository.findTopByPlayer(player);
    }
}
