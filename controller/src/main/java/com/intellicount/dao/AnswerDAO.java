package com.intellicount.dao;

import com.intellicount.model.Answer.Answer;

public interface AnswerDAO {
    Answer create(Answer answer);
}
